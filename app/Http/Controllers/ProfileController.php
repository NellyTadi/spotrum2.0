<?php

namespace App\Http\Controllers;
use App\Posttag;
use App\User;
use App\Notification;
use App\Country;
use Auth;
use Image;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $country=Country::all();
        $posttags=Posttag::orderBy('view_count','desc')->get();
        $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
    	if(auth()->user()->isAdmin == 1)
        {
           return view('admin.home',compact('posttags','notifications','country'));
        }
    	return view('profile',compact('posttags','notifications','country'));
    }
    public function update(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required|string',
            'firstname'=>'nullable|string|max:120',
            'lastname' => 'nullable|string|max:120',
            'phonenumber' => 'nullable|string',
            'dateofbirth' => 'nullable|string',
            'nationality' => 'nullable|string',
    		'post_tag_interests' => 'nullable',
    		'facebook' =>'nullable|string',
    		'twitter' =>'nullable|string',
    		'instagram' => 'nullable|string',
    		'avatar.*' => 'required|image|mimes:jpeg,png,jpg',
    	]);
    	
    	$user=User::find(Auth::user()->id);

	    	$user->name=$request->name;
            $user->firstname=$request->firstname;
            $user->lastname=$request->lastname;
            $user->phonenumber=$request->phonenumber;
            $user->dateofbirth=$request->dateofbirth;
            $user->nationality=$request->nationality;

	    	$post_tag_interests=array('post_tag_interests'=>$request->post_tag_interests);
	    	$user->post_tag_interests=json_encode($post_tag_interests);

	    	$social_media= array('facebook' =>$request->facebook ,'twitter' => $request->twitter,'instagram'=>$request->instagram );
	    	$user->social_media=json_encode($social_media);

	    	if ($request->hasfile('avatar')) {

	    		

	    		$filename=Image::make($request->file('avatar'))->resize(400,null, function ($constraint) {
				    $constraint->aspectRatio();
				    $constraint->upsize();
				});
                $thumbnail=Image::make($request->file('avatar'))->resize(100,null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
	    		$filenames=time().$request->file('avatar')->getClientOriginalName();
			    
				$filename->save('images/profile/'.$filenames);
                $thumbnail->save('images/profile/thumbnail/'.$filenames);
	    		$user->avatar=$filenames;
	    		
	    	}
	    	
    	$user->save();
    	return redirect()->back()->with('status','Profile has been updated successfully');
    }
}
