<?php

namespace App\Http\Controllers;
use App\Posttag;
use App\User;
use App\Notification;
use Auth;
use Hash;
use Illuminate\Http\Request;

class SecurityController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function show()
    {
        $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
    	$posttags=Posttag::orderBy('view_count','desc')->get();
    	return view('auth.security',compact('posttags','notifications'));
    }
    public function submit(Request $request)
    {
   
    	  if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not match with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $this->validate($request,[
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        return redirect()->back()->with("success","Password changed successfully !");
    }
    public function viewusers()
    {
        $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
        $posttags=Posttag::orderBy('view_count','desc')->get();
        $users=User::where('status',0)->get();
        return view('admin.allusers',compact('users','posttags','notifications'));
    }
    public function showuser(Request $request)
    {
        $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
        $posttags=Posttag::orderBy('view_count','desc')->get();
        $user=User::find($request->id);
        return view('admin.showuser',compact('user','posttags','notifications'));
    }
    public function deleteuser(Request $request)
    {
        $user=User::find($request->id);
        $user->status=1;
        $user->save();

        return redirect()->route('view.users')->with("success","User has been successfully deleted. User will not have access to this platform anymore.");
    }
}
