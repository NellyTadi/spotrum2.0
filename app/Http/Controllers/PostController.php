<?php

namespace App\Http\Controllers;
use App\Post;
use App\Mail\Posts;
use App\Newsletter;
use App\Posttag;
use App\User;
use App\Notification;
use App\Ads;
use Auth;
use Image;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function store(Request $request)
    {
        $this->validate($request,[
            'posttitle' => 'required|string',
            'posttags' => 'nullable',
            'postbody' => 'required',
            'postcoverimage' =>'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'youtube_video' => 'nullable|url',
        ]);

        $post= new Post;

        $post->posttitle=$request->posttitle;
        
       if ($request->hasfile('postcoverimage')) {

        $image_name = $request->file('postcoverimage');
                    $w=intval($request->w);
                    $h=intval($request->h);
                    $x1=intval($request->x1);
                    $y1=intval($request->y1);
        
                $filename=Image::make($request->file('postcoverimage'))->crop($w,$h,$x1,$y1);

                $thumbnail=Image::make($request->file('postcoverimage'))->resize(300,null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $filenames=time().$request->file('postcoverimage')->getClientOriginalName();
                
                $filename->save('post/'.$filenames);
                $thumbnail->save('post/thumbnail/'.$filenames);
                $post->postcoverimage=$filenames;
       
           if(auth()->user()->isAdmin == 1){
               $post->isAdmin=1;
            }
            else{
                 $post->isAdmin=0;
            }
         }
        
        $post->youtube_video=$request->youtube_video;
        $post->created_by=Auth::user()->name;
        $post->likes=0;
        $post->posttags=$request->posttags;
        $post->postbody=$request->postbody;
        $post->user_id=Auth::user()->id;
        $post->view_count=0;
        $post->comment_count=0;
        $post->status=0;
        $post->save();

         $newsletter= Newsletter::where('status',0)->get();
        $emails = array();
        foreach($newsletter as $news) {
         $emails[] = $news->email;
        }

         \Mail::bcc($emails)->send(new Posts($post));


        return redirect()->route('post.show',['id'=> $post->id,'title'=> $post->posttitle]);
    }

     public function viewPosts()
     {
        
        $posts=Post::where('status',0)->where('user_id',Auth::user()->id)->latest()->paginate(9);
        $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
        $ads=Ads::latest()->first();
        $posttags=Posttag::orderBy('view_count','desc')->get();
        $popularposts=Post::where('status',0)->orderBy('comment_count','desc')->limit(5)->get();
        return view('myposts',compact('posttags','posts','notifications','ads','popularposts'));
     }

    public function delete(Request $request)
    {
        $post=Post::find($request->id);
        $post->status=1;
        $post->save();
        return redirect()->back()->with('status','You have successfully deleted your post');
    }
    public function edit(Request $request)
    {
        $posttags=Posttag::orderBy('view_count','desc')->get();
        $post=Post::find($request->id);
        $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
        return view('edit',compact('post','posttags','notifications'));
    }
    public function update(Request $request)
    {
        $this->validate($request,[
            'posttitle' => 'nullable|string',
            'posttags' => 'nullable|string',
            'postbody' => 'nullable|string',
            'postcoverimage' =>'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        $post=Post::find($request->id);
        $post->posttitle=$request->posttitle;
        $post->posttags=$request->posttags;
        $post->postbody=$request->postbody;
        
         if ($request->hasfile('postcoverimage')) {
          
            $filename=time().$request->file('postcoverimage')->getClientOriginalName();
            $request->file('postcoverimage')->move('post',$filename);

            $post->postcoverimage=$filename;
           
        echo "success";
       }
      
        $post->save();

        return redirect()->route('posts.created')->with('status','You have successfully updated your post');
    }
    public function viewAllPosts()
    {
        $posttags=Posttag::orderBy('view_count','desc')->get();
        $popularposts=Post::where('status',0)->orderBy('comment_count','desc')->limit(5)->get();
        
        $posts=Post::where('status',0)->latest()->paginate(9);
        $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
        $ads=Ads::latest()->first();
        return view('admin.posts',compact('posts','posttags','notifications','ads','popularposts'));
    }
}
