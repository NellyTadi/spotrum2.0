<?php

namespace App\Http\Controllers;
use App\Ads;
use App\Posttag;
use App\Notification;
use Image;
use Illuminate\Http\Request;

class AdsController extends Controller
{
    public function show()
    {
    	$posttags=Posttag::orderBy('view_count','desc')->get();
        $notifications=Notification::where('status',0)->where('post_user_id',auth()->user()->id)->limit(10)->get();
    	return view('admin.ad',compact('posttags','notifications'));
    }
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'ad_link' => 'required|url',
    		'ad_owner'=> 'required|string',
    		'ad_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
    	]);

    	$ad=new Ads;
    	$ad->ad_owner=$request->ad_owner;
    	$ad->ad_link=$request->ad_link;
    	 if ($request->hasfile('ad_image')) {

        $image_name = $request->file('ad_image');
                  
                $thumbnail=Image::make($request->file('ad_image'))->resize(600,null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
                $filename=time().$request->file('ad_image')->getClientOriginalName();
                
                $thumbnail->save('ads/'.$filename);
           
                $ad->ad_image=$filename;
       
           
         }
    

    	$ad->save();

    	return redirect()->back()->with('status','You have successfully created Ad');
    }
}
