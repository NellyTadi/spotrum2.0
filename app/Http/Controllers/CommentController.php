<?php

namespace App\Http\Controllers;

use App\Post;
use Auth;
use App\Comment;
use App\Subcomment;
use App\Notification;
use Illuminate\Http\Request;

class CommentController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
    	$comment=new Comment;

    	$comment->post_id=$request->id;
        $comment->user_id=Auth::user()->id;
    	$comment->comment=$request->comment;
        $comment->created_by=Auth::user()->name;
        $comment->status=0;
    	$comment->save();

        $post=Post::find($request->id);
        $post->comment_count=$post->comment_count+1;
        $post->save();

        $notification=new Notification;

         
        $notification->user_id=Auth::user()->id;
        $notification->created_by=Auth::user()->name;
        $notification->notification= Auth::user()->name.' commented on your post '.$request->posttitle;
        $notification->post_id=$request->id;
        $notification->comment_id=$comment->id;
        $notification->posttitle=$request->posttitle;
        $notification->post_user_id=$request->post_user_id;
        $notification->status=0;
        $notification->save();
    	return redirect()->route('post.show',['id'=> $post->id,'title'=> $post->posttitle]);
    }

    public function storeSubcomment(Request $request)
    {
    	$comment=new Subcomment;
    	$comment->post_id=$request->id;
    	$comment->comment_id=$request->comment_id;
         $comment->user_id=Auth::user()->id;
    	$comment->comment=$request->comment;
    	$comment->created_by=Auth::user()->name;
        $comment->status=0;
        $comment->comment_user_id=$request->comment_user_id;
        $comment->privacy_status=$request->privacy_status;
    	$comment->save();
        
        $post=Post::find($request->id);
        $post->comment_count=$post->comment_count+1;
        $post->save();
        $notification=new Notification;

         
        $notification->user_id=Auth::user()->id;
        $notification->created_by=Auth::user()->name;
        $notification->notification= Auth::user()->name.' commented on your post '.$request->posttitle;
        $notification->comment_id=$comment->id;
        $notification->post_user_id=$request->post_user_id;
        $notification->post_id=$request->id;
        $notification->posttitle=$request->posttitle;
       
        $notification->status=0;
        $notification->save();
    	return redirect()->route('post.show',['id'=> $comment->post_id,'title'=> $post->posttitle]);

    }

    public function deletecomment(Request $request)
    {
        $comment=Comment::find($request->id);
        $comment->status=1;
        $comment->save();

        $comment=Subcomment::where('comment_id',$comment->id)->update(['status' => 1]);
        /*$notification=Notification::where('comment_id',$request->id)->first();
        $notification->status=1;
        $notification->save();*/
        return redirect()->back()->with('status','Comment has been deleted successfully');
    }
    public function deletesubcomment(Request $request)
    {
        $comment=Subcomment::find($request->id);
        $comment->status=1;
        $comment->save();
/*
        $notification=Notification::where('comment_id',$request->id)->first();
        $notification->status=1;
        $notification->save();*/
        return redirect()->back()->with('status','Comment has been deleted successfully');
    }
}
