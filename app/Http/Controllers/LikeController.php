<?php

namespace App\Http\Controllers;
use App\Like;
use App\Post;
use App\User;
use App\Posttag;
use App\Comment;
use App\Notification;
use App\Ads;
use Auth;
use Illuminate\Http\Request;

class LikeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show(Request $request)
    {
    	$like=Like::where('user_id',Auth::user()->id)->where('post_id',$request->id)->first();
    	if (!$like) {

    		$like=new Like;
	    	$like->user_id=Auth::user()->id;
	    	$like->post_id=$request->id;
	    	$like->save();

	    	$post=Post::where('status',0)->where('id',$request->id)->first();
	    	$post->likes=$post->likes+1;
	    	$post->save();

    	}
    	

    	$post=Post::where('status',0)->where('id',$request->id)->first();
         if (!$post) {
            return redirect()->back();
        }

        $post->view_count=$post->view_count+1;
        $post->save();
         $ads=Ads::latest()->first();
        $user=User::find($post->user_id);
        $popularposts=Post::where('status',0)->orderBy('comment_count','desc')->limit(9)->get();
        $comments=Comment::where('status',0)->where('post_id',$post->id)->get();
    	$posttags=Posttag::orderBy('view_count','desc')->get();
        if (Auth::check()) {
            $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
            return view('single',compact('post','posttags','comments','user','popularposts','notifications','ads'));
        }
        else{
        return view('single',compact('post','posttags','comments','user','popularposts','ads'));
        }
    }
}
