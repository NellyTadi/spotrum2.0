<?php

namespace App\Http\Controllers;
use App\Posttag;
use App\Notification;
use App\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function edit()
    {
        $posttags=Posttag::orderBy('view_count','desc')->get();
        if(auth()->check()){
        $notifications=Notification::where('status',0)->where('post_user_id',auth()->user()->id)->limit(10)->get();
		}
        return view('unsubscribe',compact('notifications','posttags'));
    }

    public function delete(Request $request)
    {
        $newsletter=Newsletter::where('email',$request->email)->first();
        if ($newsletter) {
        	$newsletter->status=1;
        $newsletter->save();
        }
        

        return redirect()->route('index');
    }
 
}
