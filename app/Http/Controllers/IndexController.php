<?php

namespace App\Http\Controllers;
use App\Post;
use App\User;
use App\Posttag;
use App\Comment;
use App\Notification;
use App\Ads;
use Illuminate\Http\Request;
use Auth;
class IndexController extends Controller
{
    public function index()
    {
        $ads=Ads::latest()->first();
    	$posttags=Posttag::orderBy('view_count','desc')->get();
        $popularposts=Post::where('status',0)->orderBy('comment_count','desc')->limit(5)->get(); 
    	$posts=Post::where('status',0)->latest()->limit(20)->get();
        $mostviewedpost=Post::where('status',0)->orderBy('view_count','desc')->first();
        $adminposts=Post::where('status',0)->where('isAdmin',1)->latest()->limit(3)->get();
          
        if (!Auth::check()) {
            $i=0;
            foreach ($posttags as $key) {
                if ($i==0) {
                    $footballposts=Post::where('status',0)->where('posttags',$key->tag)->limit(16)->get();
                    $key1=$key->tag;
                }
                if ($i==1) {
                    $basketballposts=Post::where('status',0)->where('posttags',$key->tag)->limit(16)->get();
                    $key2=$key->tag;
                }
                $i++;
            }
            return view('index',compact('posts','posttags','footballposts','key1','key2','basketballposts','popularposts','adminposts','mostviewedpost','ads'));
        }
        elseif (Auth::check() ) {
            $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
           
            $interests=json_decode(Auth::user()->post_tag_interests,true);
         
            if ($interests['post_tag_interests']=='' || Auth::user()->post_tag_interests== '') {
                $i=0;
            foreach ($posttags as $key) {
                if ($i==0) {
                    $footballposts=Post::where('status',0)->where('posttags',$key->tag)->limit(16)->get();
                    $key1=$key->tag;
                }
                if ($i==1) {
                    $basketballposts=Post::where('status',0)->where('posttags',$key->tag)->limit(16)->get();
                    $key2=$key->tag;
                }
                $i++;
            }
            return view('index',compact('posts','posttags','footballposts','key1','key2','basketballposts','popularposts','adminposts','notifications','mostviewedpost','ads'));
            
            }
            else{
            $i=0;
            foreach ($interests['post_tag_interests'] as $key) {
                if ($i==0) {
                    $firstauthposts=Post::where('status',0)->where('posttags',$key)->get();
                    $key1=$key;
                }
                 
                if ($i==1) {
                    $secondauthposts=Post::where('status',0)->where('posttags',$key)->get();
                    $key2=$key;
                }
               $i++;
            
            }
          
         return view('index',compact('posts','posttags','firstauthposts','secondauthposts','key1','key2','popularposts','adminposts','notifications','mostviewedpost','ads'));
            }
        }
       

       
    	 

    }
    public function show(Request $request)
    {
    	$post=Post::where('status',0)->where('id',$request->id)->first();
         if (!isset($post)) {
            return redirect()->back();
        }

        $post->view_count=$post->view_count+1;
        $post->save();
         $ads=Ads::latest()->first();
        $user=User::find($post->user_id);
        $popularposts=Post::where('status',0)->orderBy('comment_count','desc')->limit(9)->get();
        $comments=Comment::where('status',0)->where('post_id',$post->id)->get();
    	$posttags=Posttag::orderBy('view_count','desc')->get();
        if (Auth::check()) {
            $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
            return view('single',compact('post','posttags','comments','user','popularposts','notifications','ads'));
        }
        else{
        return view('single',compact('post','posttags','comments','user','popularposts','ads'));
        }
    }
    public function search(Request $request)
    {
         $ads=Ads::latest()->first();
         $posttags=Posttag::orderBy('view_count','desc')->get();
        $popularposts=Post::where('status',0)->orderBy('comment_count','desc')->limit(5)->get();
         $posts=Post::where('status',0)->where('posttags','like',"%$request->tag%")->paginate(12);
        if (count($posts) < 1) {
            $posts=Post::where('status',0)->where('posttitle','like',"%$request->tag%")->paginate(12);
            if (count($posts) < 1) {
                 $posts=Post::where('status',0)->where('postbody','like',"%$request->tag%")->paginate(12);
                 if (count($posts) < 1) {
                     $posts=Post::where('status',0)->paginate(12);
                     $noresult= 'No related result found';
                 }
            }
            
        }
        else{
            $posttag=Posttag::where('tag','like',"%$request->tag%")->first();
        $posttag->view_count=$posttag->view_count+1;
        $posttag->save();

        }
        
        $latestposts=Post::where('status',0)->latest()->limit(3)->get();
        $tag=$request->tag;
        if (Auth::check()) {
            $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
            return view('search',compact('posts','posttags','tag','latestposts','notifications','noresult','ads','popularposts'));
        }
        else{
            return view('search',compact('posts','posttags','tag','latestposts','noresult','ads','popularposts'));
        }
    }
}
