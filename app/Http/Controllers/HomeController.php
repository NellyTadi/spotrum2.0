<?php

namespace App\Http\Controllers;
use App\Post;
use App\Posttag;
use App\Notification;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posttags=Posttag::orderBy('view_count','desc')->get();
        $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
       if(auth()->user()->isAdmin == 1){
           return view('admin.home',compact('posttags','notifications'));
       }
       
       else{
        return view('home',compact('posttags','notifications'));
        }
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   
}
