<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\Newsletter;
use App\Mail\Welcome;
use App\User;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the Social Media authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider($social)
    {
        return Socialite::driver($social)->redirect();
    }

    /**
     * Obtain the user information from Social Media.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($social)
    {
        $userInfo = Socialite::driver($social)->user();
        //confirm if user exists else create user
        $user=User::where('email',$userInfo->email)->first();
 
        if ($user) {
           if(Auth::loginUsingId($user->id)){

                  return redirect()->route('home');
            }
           
        }
        else{

            $userCreate= User::create([
            
                'name' => $userInfo->name,
                'email' => $userInfo->email,
                'provider'=> $social,
                'password' => bcrypt('social'),
                'status' => 0,
        ]);


        $newsletter=Newsletter::where('email',$userInfo->email)->first();
        $name=explode("@", $userInfo->email);
        if(!$newsletter){
            $newsletter= new Newsletter;
            $newsletter->email=$userInfo->email;
            $newsletter->name=$name[0];
            $newsletter->status=0;
            $newsletter->save();
             \Mail::to($userInfo->email)->send(new Welcome($newsletter));
        }
        
          Auth::login($userCreate,true);
             return redirect()->route('home');
          

        }
    }
}
