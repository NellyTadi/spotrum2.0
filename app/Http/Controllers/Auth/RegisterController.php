<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Posttag;
use App\Newsletter;
use App\Mail\Welcome;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function showAdminRegistrationForm()
    {
        $posttags=Posttag::orderBy('view_count','desc')->get();

        return view('auth.admin-register',compact('posttags'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'isAdmin'=> 'nullable',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $name=explode("@", $data['email']);

        $newsletter=Newsletter::where('email',$data['email'])->first();
        if(!$newsletter){
            $newsletter= new Newsletter;
            $newsletter->email=$data['email'];
            $newsletter->name=$name[0];
            $newsletter->status=0;
            $newsletter->save();
             \Mail::to($data['email'])->send(new Welcome($newsletter));
        }

        
        if (isset($data['isAdmin'])) {
             return User::create([
            'name' => $name[0],
            'email' => $data['email'],
            'isAdmin' => $data['isAdmin'],
            'password' => bcrypt($data['password']),
            'status' => 0,
        ]);
        }
        else{
             return User::create([
            'name' => $name[0],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'status' => 0,
        ]);
        }


       
    }
}
