<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function subcomments()
    {
    	return $this->hasMany('App\Subcomment')->where('status',0 );
    }
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
