-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 24, 2019 at 02:33 PM
-- Server version: 5.6.44
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spotrumc_spotrum`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(10) UNSIGNED NOT NULL,
  `ad_owner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ad_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country`, `country_code`) VALUES
(1, 'Afganistan', 'AF'),
(2, 'Aland Islands', 'AX'),
(3, 'Albania', 'AL'),
(4, 'Algeria', 'DZ'),
(5, 'American Samoa', 'AS'),
(6, 'Andorra', 'AD'),
(7, 'Angola', 'AO'),
(8, 'Anguilla', 'AI'),
(9, 'Antarctica', 'AQ'),
(10, 'Antigua and Barbuda', 'AG'),
(11, 'Argentina', 'AR'),
(12, 'Armenia', 'AM'),
(13, 'Aruba', 'AW'),
(14, 'Australia', 'AU'),
(15, 'Austria', 'AT'),
(16, 'Azerbaijan', 'AZ'),
(17, 'Bahamas', 'BS'),
(18, 'Bahrain', 'BH'),
(19, 'Bangladesh', 'BD'),
(20, 'Barbados', 'BB'),
(21, 'Belarus', 'BY'),
(22, 'Belgium', 'BE'),
(23, 'Belize', 'BZ'),
(24, 'Benin', 'BJ'),
(25, 'Bermuda', 'BM'),
(26, 'Bhutan', 'BT'),
(27, 'Bolivia, Plurinational State of', 'BO'),
(28, 'Bonaire, Sint Eustatius and Saba', 'BQ'),
(29, 'Bosnia and Herzegovina', 'BA'),
(30, 'Botswana', 'BW'),
(31, 'Bouvet Island', 'BV'),
(32, 'Brazil', 'BR'),
(33, 'British Indian Ocean Territory', 'IO'),
(34, 'Brunei Darussalam', 'BN'),
(35, 'Bulgaria', 'BG'),
(36, 'Burkina Faso', 'BF'),
(37, 'Burundi', 'BI'),
(38, 'Cambodia', 'KH'),
(39, 'Cameroon', 'CM'),
(40, 'Canada', 'CA'),
(41, 'Cape Verde', 'CV'),
(42, 'Cayman Islands', 'KY'),
(43, 'Central African Republic', 'CF'),
(44, 'Chad', 'TD'),
(45, 'Chile', 'CL'),
(46, 'China', 'CN'),
(47, 'Christmas Island', 'CX'),
(48, 'Cocos (Keeling) Islands', 'CC'),
(49, 'Colombia', 'CO'),
(50, 'Comoros', 'KM'),
(51, 'Congo', 'CG'),
(52, 'Congo, the Democratic Republic of the', 'CD'),
(53, 'Cook Islands', 'CK'),
(54, 'Costa Rica', 'CR'),
(55, 'C?te d\'Ivoire', 'CI'),
(56, 'Croatia', 'HR'),
(57, 'Cuba', 'CU'),
(58, 'Cura?ao', 'CW'),
(59, 'Cyprus', 'CY'),
(60, 'Czech Republic', 'CZ'),
(61, 'Denmark', 'DK'),
(62, 'Djibouti', 'DJ'),
(63, 'Dominica', 'DM'),
(64, 'Dominican Republic', 'DO'),
(65, 'Ecuador', 'EC'),
(66, 'Egypt', 'EG'),
(67, 'El Salvador', 'SV'),
(68, 'Equatorial Guinea', 'GQ'),
(69, 'Eritrea', 'ER'),
(70, 'Estonia', 'EE'),
(71, 'Ethiopia', 'ET'),
(72, 'Falkland Islands (Malvinas)', 'FK'),
(73, 'Faroe Islands', 'FO'),
(74, 'Fiji', 'FJ'),
(75, 'Finland', 'FI'),
(76, 'France', 'FR'),
(77, 'French Guiana', 'GF'),
(78, 'French Polynesia', 'PF'),
(79, 'French Southern Territories', 'TF'),
(80, 'Gabon', 'GA'),
(81, 'Gambia', 'GM'),
(82, 'Georgia', 'GE'),
(83, 'Germany', 'DE'),
(84, 'Ghana', 'GH'),
(85, 'Gibraltar', 'GI'),
(86, 'Greece', 'GR'),
(87, 'Greenland', 'GL'),
(88, 'Grenada', 'GD'),
(89, 'Guadeloupe', 'GP'),
(90, 'Guam', 'GU'),
(91, 'Guatemala', 'GT'),
(92, 'Guernsey', 'GG'),
(93, 'Guinea', 'GN'),
(94, 'Guinea-Bissau', 'GW'),
(95, 'Guyana', 'GY'),
(96, 'Haiti', 'HT'),
(97, 'Heard Island and McDonald Islands', 'HM'),
(98, 'Holy See (Vatican City State)', 'VA'),
(99, 'Honduras', 'HN'),
(100, 'Hong Kong', 'HK'),
(101, 'Hungary', 'HU'),
(102, 'Iceland', 'IS'),
(103, 'India', 'IN'),
(104, 'Indonesia', 'ID'),
(105, 'Iran, Islamic Republic of', 'IR'),
(106, 'Iraq', 'IQ'),
(107, 'Ireland', 'IE'),
(108, 'Isle of Man', 'IM'),
(109, 'Israel', 'IL'),
(110, 'Italy', 'IT'),
(111, 'Jamaica', 'JM'),
(112, 'Japan', 'JP'),
(113, 'Jersey', 'JE'),
(114, 'Jordan', 'JO'),
(115, 'Kazakhstan', 'KZ'),
(116, 'Kenya', 'KE'),
(117, 'Kiribati', 'KI'),
(118, 'Korea, Democratic People\'s Republic of', 'KP'),
(119, 'Korea, Republic of', 'KR'),
(120, 'Kuwait', 'KW'),
(121, 'Kyrgyzstan', 'KG'),
(122, 'Lao People\'s Democratic Republic', 'LA'),
(123, 'Latvia', 'LV'),
(124, 'Lebanon', 'LB'),
(125, 'Lesotho', 'LS'),
(126, 'Liberia', 'LR'),
(127, 'Libya', 'LY'),
(128, 'Liechtenstein', 'LI'),
(129, 'Lithuania', 'LT'),
(130, 'Luxembourg', 'LU'),
(131, 'Macao', 'MO'),
(132, 'Macedonia, the Former Yugoslav Republic of', 'MK'),
(133, 'Madagascar', 'MG'),
(134, 'Malawi', 'MW'),
(135, 'Malaysia', 'MY'),
(136, 'Maldives', 'MV'),
(137, 'Mali', 'ML'),
(138, 'Malta', 'MT'),
(139, 'Marshall Islands', 'MH'),
(140, 'Martinique', 'MQ'),
(141, 'Mauritania', 'MR'),
(142, 'Mauritius', 'MU'),
(143, 'Mayotte', 'YT'),
(144, 'Mexico', 'MX'),
(145, 'Micronesia, Federated States of', 'FM'),
(146, 'Moldova, Republic of', 'MD'),
(147, 'Monaco', 'MC'),
(148, 'Mongolia', 'MN'),
(149, 'Montenegro', 'ME'),
(150, 'Montserrat', 'MS'),
(151, 'Morocco', 'MA'),
(152, 'Mozambique', 'MZ'),
(153, 'Myanmar', 'MM'),
(154, 'Namibia', 'NA'),
(155, 'Nauru', 'NR'),
(156, 'Nepal', 'NP'),
(157, 'Netherlands', 'NL'),
(158, 'New Caledonia', 'NC'),
(159, 'New Zealand', 'NZ'),
(160, 'Nicaragua', 'NI'),
(161, 'Niger', 'NE'),
(162, 'Nigeria', 'NG'),
(163, 'Niue', 'NU'),
(164, 'Norfolk Island', 'NF'),
(165, 'Northern Mariana Islands', 'MP'),
(166, 'Norway', 'NO'),
(167, 'Oman', 'OM'),
(168, 'Pakistan', 'PK'),
(169, 'Palau', 'PW'),
(170, 'Palestine, State of', 'PS'),
(171, 'Panama', 'PA'),
(172, 'Papua New Guinea', 'PG'),
(173, 'Paraguay', 'PY'),
(174, 'Peru', 'PE'),
(175, 'Philippines', 'PH'),
(176, 'Pitcairn', 'PN'),
(177, 'Poland', 'PL'),
(178, 'Portugal', 'PT'),
(179, 'Puerto Rico', 'PR'),
(180, 'Qatar', 'QA'),
(181, 'R?union', 'RE'),
(182, 'Romania', 'RO'),
(183, 'Russian Federation', 'RU'),
(184, 'Rwanda', 'RW'),
(185, 'Saint Barth?lemy', 'BL'),
(186, 'Saint Helena, Ascension and Tristan da Cunha', 'SH'),
(187, 'Saint Kitts and Nevis', 'KN'),
(188, 'Saint Lucia', 'LC'),
(189, 'Saint Martin (French part)', 'MF'),
(190, 'Saint Pierre and Miquelon', 'PM'),
(191, 'Saint Vincent and the Grenadines', 'VC'),
(192, 'Samoa', 'WS'),
(193, 'San Marino', 'SM'),
(194, 'Sao Tome and Principe', 'ST'),
(195, 'Saudi Arabia', 'SA'),
(196, 'Senegal', 'SN'),
(197, 'Serbia', 'RS'),
(198, 'Seychelles', 'SC'),
(199, 'Sierra Leone', 'SL'),
(200, 'Singapore', 'SG'),
(201, 'Sint Maarten (Dutch part)', 'SX'),
(202, 'Slovakia', 'SK'),
(203, 'Slovenia', 'SI'),
(204, 'Solomon Islands', 'SB'),
(205, 'Somalia', 'SO'),
(206, 'South Africa', 'ZA'),
(207, 'South Georgia and the South Sandwich Islands', 'GS'),
(208, 'South Sudan', 'SS'),
(209, 'Spain', 'ES'),
(210, 'Sri Lanka', 'LK'),
(211, 'Sudan', 'SD'),
(212, 'Suriname', 'SR'),
(213, 'Svalbard and Jan Mayen', 'SJ'),
(214, 'Swaziland', 'SZ'),
(215, 'Sweden', 'SE'),
(216, 'Switzerland', 'CH'),
(217, 'Syrian Arab Republic', 'SY'),
(218, 'Taiwan, Province of China', 'TW'),
(219, 'Tajikistan', 'TJ'),
(220, 'Tanzania, United Republic of', 'TZ'),
(221, 'Thailand', 'TH'),
(222, 'Timor-Leste', 'TL'),
(223, 'Togo', 'TG'),
(224, 'Tokelau', 'TK'),
(225, 'Tonga', 'TO'),
(226, 'Trinidad and Tobago', 'TT'),
(227, 'Tunisia', 'TN'),
(228, 'Turkey', 'TR'),
(229, 'Turkmenistan', 'TM'),
(230, 'Turks and Caicos Islands', 'TC'),
(231, 'Tuvalu', 'TV'),
(232, 'Uganda', 'UG'),
(233, 'Ukraine', 'UA'),
(234, 'United Arab Emirates', 'AE'),
(235, 'United Kingdom', 'GB'),
(236, 'United States', 'US'),
(237, 'United States Minor Outlying Islands', 'UM'),
(238, 'Uruguay', 'UY'),
(239, 'Uzbekistan', 'UZ'),
(240, 'Vanuatu', 'VU'),
(241, 'Venezuela, Bolivarian Republic of', 'VE'),
(242, 'Viet Nam', 'VN'),
(243, 'Virgin Islands, British', 'VG'),
(244, 'Virgin Islands, U.S.', 'VI'),
(245, 'Wallis and Futuna', 'WF'),
(246, 'Western Sahara', 'EH'),
(247, 'Yemen', 'YE'),
(248, 'Zambia', 'ZM'),
(249, 'Zimbabwe', 'ZW');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `user_id`, `post_id`, `created_at`, `updated_at`) VALUES
(1, 8, 1, '2019-05-13 10:12:08', '2019-05-13 10:12:08'),
(2, 4, 9, '2019-06-17 11:48:40', '2019-06-17 11:48:40'),
(3, 9, 12, '2019-06-24 03:10:39', '2019-06-24 03:10:39');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_21_220503_create_posts_table', 2),
(4, '2018_11_27_164356_create_posttags_table', 3),
(5, '2018_11_28_231123_create_comments_table', 4),
(6, '2018_11_29_000745_create_subcomments_table', 5),
(7, '2018_11_29_235013_test', 6),
(9, '2018_12_20_192708_create_notifications_table', 7),
(10, '2019_02_19_230516_create_ads_table', 8),
(11, '2019_02_23_072322_create_countries_table', 9),
(12, '2019_02_27_133023_create_newsletters_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `newsletters`
--

INSERT INTO `newsletters` (`id`, `email`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin@spotrum.com', 'admin', 0, '2019-03-06 14:38:48', '2019-03-06 14:38:48'),
(2, 'nellytadi@gmail.com', 'nellytadi', 0, '2019-03-07 00:10:34', '2019-03-07 00:10:34'),
(3, 'nathandauda27@gmail.com', 'nathandauda27', 0, '2019-03-07 01:35:06', '2019-03-07 01:35:06'),
(4, 'spotrum@gmail.com', 'spotrum', 0, '2019-03-13 20:33:34', '2019-03-13 20:33:34'),
(5, 'rickiedominic@yahoo.com', 'rickiedominic', 0, '2019-03-20 12:17:40', '2019-03-20 12:17:40'),
(6, 'dezukanma@hotmail.com', 'dezukanma', 0, '2019-03-29 22:29:25', '2019-03-29 22:29:25'),
(7, 'toluolayinka@gmail.com', 'toluolayinka', 0, '2019-04-07 16:46:09', '2019-04-07 16:46:09'),
(8, 'marcusaken@gmail.com', 'marcusaken', 0, '2019-05-08 09:25:50', '2019-05-08 09:25:50'),
(9, 'okoloesthermary@gmail.com', 'okoloesthermary', 0, '2019-06-24 02:37:21', '2019-06-24 02:37:21');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_user_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `posttitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `posttitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posttags` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postbody` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcoverimage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube_video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_count` int(11) NOT NULL,
  `comment_count` int(11) NOT NULL,
  `likes` int(11) NOT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `posttitle`, `posttags`, `postbody`, `postcoverimage`, `youtube_video`, `view_count`, `comment_count`, `likes`, `isAdmin`, `status`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'History offers little hope for Man United as they face the fearless PSG', 'Football', '<p>Ahead of Wednesday\'s Champions League matches, we look at the key Opta numbers.</p><p>History is stacked against Manchester United in their bid to overturn a 2-0 first-leg home defeat to reach the Champions League quarter-finals at the expense of Paris Saint-Germain on Wednesday.</p><p>Ole Gunnar Solskjaer\'s United travel to Paris without 10 injured first-team players and the suspended Paul Pogba, meaning five youth players have been drafted into the squad.</p><p>PSG hold a handsome&nbsp;lead thanks to first-leg goals from Presnel Kimpembe and Kylian Mbappe, while United\'s recent record in Champions League knockout games provides little source for optimism.</p><p>On top of that, no team has ever progressed from a knockout tie in the Champions League after losing the first leg at home by two goals or more.</p><p>Wednesday\'s other contest sees Porto host Roma hoping to overturn a 2-1 defeat away in Rome last month, and below are the key Opta numbers ahead of the two matches.</p>', '1551887735man-utd-cropped_1nhsxgtbglkll1vwcrb4763k9v.jpg', NULL, 134, 0, 1, 1, 0, 'admin', '2019-03-06 14:55:35', '2019-06-24 02:54:58'),
(2, 1, 'Quadri climbs to 22 on ITTF World Ranking', 'Table Tennis', '<p class=\"ql-align-justify\">DESPITE not being able to take Nigeria to the Championship Division of the 2018 World Team Championships, Aruna Quadri’s performance in Sweden earned his rise in the May 2018 ITTF World Ranking.</p><p class=\"ql-align-justify\">Like Quadri, his compatriots – Segun Toriola, Olajide Omotayo and Bode Abiodun also gained places in the latest rating released yesterday by the world table tennis ruling body.</p><p class=\"ql-align-justify\">Having featured in all the seven matches played by Nigeria in Halmstad with a lost to Slovakia’s Lubomir Pistej in the quarter-final of the men’s second division, Quadri gained four places from 26 to rise to 22 in the May ranking.</p><p class=\"ql-align-justify\">Like Quadri, Toriola, Omotayo and Abiodun also got improved ranking with Toriola rising to 157 from 166.</p><p class=\"ql-align-justify\">Omotayo who has continued to gain places in the world rating also rose to 179 from 215, while Abiodun moved from 296 to 266.</p><p class=\"ql-align-justify\">The improved individual ranking has also rubbed off positively on the team ranking as Nigeria now occupies a joint 26th place with Chile and Denmark.</p><p class=\"ql-align-justify\">The poor outing of Egypt did not affect the fortunes of Omar Assar as the African champion maintains his status as number 17 in the latest ranking.</p><p class=\"ql-align-justify\">The Most Valuable Player (MVP) at the conclude World team Championship, China’s Fan Zhendong maintains his status as the world number one, while Germany’s duo of Dimitrij Ovtcharov and Timo Boll occupy second and third places in the latest ranking.</p><p class=\"ql-align-justify\">For emerging as champion in Sweden, China has displaced Germany as the number one seed team in the world. Japan is second, while Hong Kong occupies the third spot in the rating.</p><p><br></p>', '1551895575Aruna-Quadri7.jpg', NULL, 107, 0, 0, 1, 0, 'admin', '2019-03-06 17:06:15', '2019-06-23 12:38:51'),
(3, 1, 'Bogut receives clearance to return to NBA', 'Football', '<p><br></p>', NULL, NULL, 1, 0, 0, NULL, 1, 'admin', '2019-03-06 17:26:35', '2019-03-06 17:28:08'),
(4, 1, 'Bogut receives clearance to return to NBA', 'Football', '<p><br></p>', NULL, NULL, 0, 0, 0, NULL, 1, 'admin', '2019-03-06 17:26:35', '2019-03-06 17:28:04'),
(5, 1, 'Bogut finally receives clearance to return to NBA', 'Basketball', '<h2>Big man expected to sign deal with Warriors within next day or two</h2><p><strong>From NBA Twitter and media reports</strong></p><p><br></p><p><br></p><p>Golden State Warriors fans will soon see a familiar face on the roster.</p><p>Per Shams Charania of The Athletic, the Warriors and center Andrew Bogut has received his letter of clearance from Austrailia\'s National Basketball League and the NBA. That document allows Bogut and the Warriors to work on finalizing a one-year deal that will put him back on the NBA team he spent the 2012-16 seasons with.</p><p>Per Mark Medina of The Mercury News, the Warriors are hoping to sign Bogut to his deal in the next day or two.</p><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong><img src=\"https://pbs.twimg.com/profile_images/1001539429628964864/3HBScTqM_normal.jpg\"></strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong>Shams Charania</strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong>✔</strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: rgb(105, 120, 130);\"><strong>@ShamsCharania</strong></a></blockquote><blockquote><br></blockquote><blockquote>Andrew Bogut has agreed to return to the Golden State Warriors, and has received his letter of clearance from Australia and the NBA, agent David Bauman of ISE tells <a href=\"https://twitter.com/TheAthleticNBA\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">@TheAthleticNBA</a> <a href=\"https://twitter.com/Stadium\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">@Stadium</a>.</blockquote><blockquote><br></blockquote><blockquote><a href=\"https://twitter.com/intent/like?tweet_id=1103325509721694208\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">2,008</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: inherit;\"><strong>5:04 PM - Mar 6, 2019</strong></a></blockquote><blockquote><a href=\"https://support.twitter.com/articles/20175256\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">Twitter Ads info and privacy</a></blockquote><p><br></p><p><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">716 people are talking about this</a></p><p><br></p><p><br></p><p><br></p><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong><img src=\"https://pbs.twimg.com/profile_images/1001539429628964864/3HBScTqM_normal.jpg\"></strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong>Shams Charania</strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong>✔</strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: rgb(105, 120, 130);\"><strong>@ShamsCharania</strong></a></blockquote><blockquote><br></blockquote><blockquote>Bogut and the Warriors will now work to finalize a one-year, minimum contract. The NBL’s MVP is expected to hold a formal press conference soon with the Sydney Kings to announce his move.</blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">Shams Charania</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">✔</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(105, 120, 130);\">@ShamsCharania</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">Andrew Bogut has agreed to return to the Golden State Warriors, and has received his letter of clearance from Australia and the NBA, agent David Bauman of ISE tells @TheAthleticNBA @Stadium.</a></blockquote><blockquote><br></blockquote><blockquote><a href=\"https://twitter.com/intent/like?tweet_id=1103325727993274369\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">516</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325727993274369\" target=\"_blank\" style=\"color: inherit;\"><strong>5:05 PM - Mar 6, 2019</strong></a></blockquote><blockquote><a href=\"https://support.twitter.com/articles/20175256\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">Twitter Ads info and privacy</a></blockquote><p><br></p><p><a href=\"https://twitter.com/ShamsCharania/status/1103325727993274369\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">167 people are talking about this</a></p><p><br></p><p><br></p><p><br></p><blockquote><a href=\"https://twitter.com/MarkG_Medina\" target=\"_blank\" style=\"color: inherit;\"><strong><img src=\"https://pbs.twimg.com/profile_images/776516261022879744/vFNHaRCZ_normal.jpg\"></strong></a></blockquote><blockquote><a href=\"https://twitter.com/MarkG_Medina\" target=\"_blank\" style=\"color: inherit;\"><strong>Mark Medina</strong></a></blockquote><blockquote><a href=\"https://twitter.com/MarkG_Medina\" target=\"_blank\" style=\"color: inherit;\"><strong>✔</strong></a></blockquote><blockquote><a href=\"https://twitter.com/MarkG_Medina\" target=\"_blank\" style=\"color: rgb(105, 120, 130);\"><strong>@MarkG_Medina</strong></a></blockquote><blockquote><br></blockquote><blockquote>Warriors hope to sign Andrew Bogut\'s contract in the next day or two, sources day. Then the Warriors will have more clarity when he will join the team.</blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">Shams Charania</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">✔</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(105, 120, 130);\">@ShamsCharania</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">Andrew Bogut has agreed to return to the Golden State Warriors, and has received his letter of clearance from Australia and the NBA, agent David Bauman of ISE tells @TheAthleticNBA @Stadium.</a></blockquote><blockquote><br></blockquote><blockquote><a href=\"https://twitter.com/intent/like?tweet_id=1103328334262329344\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">43</a></blockquote><blockquote><a href=\"https://twitter.com/MarkG_Medina/status/1103328334262329344\" target=\"_blank\" style=\"color: inherit;\"><strong>5:15 PM - Mar 6, 2019</strong></a></blockquote><blockquote><a href=\"https://support.twitter.com/articles/20175256\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">Twitter Ads info and privacy</a></blockquote><p><br></p><p><a href=\"https://twitter.com/MarkG_Medina\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">See Mark Medina\'s other Tweets</a></p><p><br></p><p><br></p><p><br></p><p>Originally drafted first overall&nbsp;<a href=\"https://stats.nba.com/draft/history/?Season=2005\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">by Milwaukee in 2005</a>, the 7-foot big man holds career averages of 9.8 points, 8.7 rebounds and 1.6 blocks per game. He was a key member of&nbsp;<a href=\"http://www.nba.com/playoffs/2015/finals/\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">the Warriors\' 2015 championship team</a>&nbsp;as well as the team that&nbsp;<a href=\"http://www.nba.com/playoffs/2016/finals/\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">reached the 2016 Finals</a>. Golden State lost the \'16 series to the Cleveland Cavaliers as Bogut missed the final two games of that series&nbsp;<a href=\"http://www.nba.com/2016/news/06/15/warriors-bogut-out-for-finals.ap/\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">with a knee injury</a>.</p><p>Bogut&nbsp;most recently played for the Lakers in 2017-18, is coming off a standout NBL campaign&nbsp;in which he won the league\'s Most Valuable Player and Defensive Player of the Year awards. He signed&nbsp;<a href=\"http://www.nba.com/article/2018/04/23/andrew-bogut-retires-nba-joins-australia-nbl\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">with the NBL last April</a>&nbsp;and planned to play two seasons with the Kings, saying then there were&nbsp;\"no outs\" in his deal if an NBA team should become interested.</p><p>After being waived by the&nbsp;Lakers in January of 2018, it was expected the free agent would join a contending team for a playoff push. But Bogut said at the time he\'d stay in Australia and focus on returning in the 2018-19 season. He also tweeted that his wife\'s pregnancy with their second child has been deemed \"high-risk\" and she was&nbsp;not allowed to travel at the time.</p><p>Bogut played 24 games for the Lakers after signing in September of 2017, averaging 1.5 points and 3.3 rebounds while starting five games. He was acquired by Cleveland in the second half of last season, but he fractured his left leg less than a minute into his debut with the Cavaliers. He played seven years with the Bucks (2005-12) before being traded to Golden State&nbsp;<a href=\"https://www.nba.com/warriors/news/warriors_acquire_bogut_jackson.html\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">in the Monta Ellis deal</a>.</p><p><em>Information from The Associated Press was used in this report.</em></p>', '1551897005andrew-bogut-ball-iso-0306.jpg', NULL, 83, 0, 0, 1, 0, 'admin', '2019-03-06 17:30:05', '2019-06-16 07:53:20'),
(6, 1, 'Bogut finally receives clearance to return to NBA', 'Basketball', '<h2>Big man expected to sign deal with Warriors within next day or two</h2><p><strong>From NBA Twitter and media reports</strong></p><p><br></p><p>Golden State Warriors fans will soon see a familiar face on the roster.</p><p>Per Shams Charania of The Athletic, the Warriors and center Andrew Bogut has received his letter of clearance from Austrailia\'s National Basketball League and the NBA. That document allows Bogut and the Warriors to work on finalizing a one-year deal that will put him back on the NBA team he spent the 2012-16 seasons with.</p><p>Per Mark Medina of The Mercury News, the Warriors are hoping to sign Bogut to his deal in the next day or two.</p><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong><img src=\"https://pbs.twimg.com/profile_images/1001539429628964864/3HBScTqM_normal.jpg\"></strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong>Shams Charania</strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong>✔</strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: rgb(105, 120, 130);\"><strong>@ShamsCharania</strong></a></blockquote><blockquote><br></blockquote><blockquote>Andrew Bogut has agreed to return to the Golden State Warriors, and has received his letter of clearance from Australia and the NBA, agent David Bauman of ISE tells <a href=\"https://twitter.com/TheAthleticNBA\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">@TheAthleticNBA</a> <a href=\"https://twitter.com/Stadium\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">@Stadium</a>.</blockquote><blockquote><br></blockquote><blockquote><a href=\"https://twitter.com/intent/like?tweet_id=1103325509721694208\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">2,008</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: inherit;\"><strong>5:04 PM - Mar 6, 2019</strong></a></blockquote><blockquote><a href=\"https://support.twitter.com/articles/20175256\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">Twitter Ads info and privacy</a></blockquote><p><br></p><p><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">716 people are talking about this</a></p><p><br></p><p><br></p><p><br></p><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong><img src=\"https://pbs.twimg.com/profile_images/1001539429628964864/3HBScTqM_normal.jpg\"></strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong>Shams Charania</strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: inherit;\"><strong>✔</strong></a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania\" target=\"_blank\" style=\"color: rgb(105, 120, 130);\"><strong>@ShamsCharania</strong></a></blockquote><blockquote><br></blockquote><blockquote>Bogut and the Warriors will now work to finalize a one-year, minimum contract. The NBL’s MVP is expected to hold a formal press conference soon with the Sydney Kings to announce his move.</blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">Shams Charania</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">✔</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(105, 120, 130);\">@ShamsCharania</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">Andrew Bogut has agreed to return to the Golden State Warriors, and has received his letter of clearance from Australia and the NBA, agent David Bauman of ISE tells @TheAthleticNBA @Stadium.</a></blockquote><blockquote><br></blockquote><blockquote><a href=\"https://twitter.com/intent/like?tweet_id=1103325727993274369\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">516</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325727993274369\" target=\"_blank\" style=\"color: inherit;\"><strong>5:05 PM - Mar 6, 2019</strong></a></blockquote><blockquote><a href=\"https://support.twitter.com/articles/20175256\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">Twitter Ads info and privacy</a></blockquote><p><br></p><p><a href=\"https://twitter.com/ShamsCharania/status/1103325727993274369\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">167 people are talking about this</a></p><p><br></p><p><br></p><p><br></p><blockquote><a href=\"https://twitter.com/MarkG_Medina\" target=\"_blank\" style=\"color: inherit;\"><strong><img src=\"https://pbs.twimg.com/profile_images/776516261022879744/vFNHaRCZ_normal.jpg\"></strong></a></blockquote><blockquote><a href=\"https://twitter.com/MarkG_Medina\" target=\"_blank\" style=\"color: inherit;\"><strong>Mark Medina</strong></a></blockquote><blockquote><a href=\"https://twitter.com/MarkG_Medina\" target=\"_blank\" style=\"color: inherit;\"><strong>✔</strong></a></blockquote><blockquote><a href=\"https://twitter.com/MarkG_Medina\" target=\"_blank\" style=\"color: rgb(105, 120, 130);\"><strong>@MarkG_Medina</strong></a></blockquote><blockquote><br></blockquote><blockquote>Warriors hope to sign Andrew Bogut\'s contract in the next day or two, sources day. Then the Warriors will have more clarity when he will join the team.</blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">Shams Charania</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">✔</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(105, 120, 130);\">@ShamsCharania</a></blockquote><blockquote><a href=\"https://twitter.com/ShamsCharania/status/1103325509721694208\" target=\"_blank\" style=\"color: rgb(28, 32, 34);\">Andrew Bogut has agreed to return to the Golden State Warriors, and has received his letter of clearance from Australia and the NBA, agent David Bauman of ISE tells @TheAthleticNBA @Stadium.</a></blockquote><blockquote><br></blockquote><blockquote><a href=\"https://twitter.com/intent/like?tweet_id=1103328334262329344\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">43</a></blockquote><blockquote><a href=\"https://twitter.com/MarkG_Medina/status/1103328334262329344\" target=\"_blank\" style=\"color: inherit;\"><strong>5:15 PM - Mar 6, 2019</strong></a></blockquote><blockquote><a href=\"https://support.twitter.com/articles/20175256\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">Twitter Ads info and privacy</a></blockquote><p><br></p><p><a href=\"https://twitter.com/MarkG_Medina\" target=\"_blank\" style=\"color: rgb(43, 123, 185);\">See Mark Medina\'s other Tweets</a></p><p><br></p><p><br></p><p><br></p><p>Originally drafted first overall&nbsp;<a href=\"https://stats.nba.com/draft/history/?Season=2005\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">by Milwaukee in 2005</a>, the 7-foot big man holds career averages of 9.8 points, 8.7 rebounds and 1.6 blocks per game. He was a key member of&nbsp;<a href=\"http://www.nba.com/playoffs/2015/finals/\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">the Warriors\' 2015 championship team</a>&nbsp;as well as the team that&nbsp;<a href=\"http://www.nba.com/playoffs/2016/finals/\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">reached the 2016 Finals</a>. Golden State lost the \'16 series to the Cleveland Cavaliers as Bogut missed the final two games of that series&nbsp;<a href=\"http://www.nba.com/2016/news/06/15/warriors-bogut-out-for-finals.ap/\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">with a knee injury</a>.</p><p>Bogut&nbsp;most recently played for the Lakers in 2017-18, is coming off a standout NBL campaign&nbsp;in which he won the league\'s Most Valuable Player and Defensive Player of the Year awards. He signed&nbsp;<a href=\"http://www.nba.com/article/2018/04/23/andrew-bogut-retires-nba-joins-australia-nbl\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">with the NBL last April</a>&nbsp;and planned to play two seasons with the Kings, saying then there were&nbsp;\"no outs\" in his deal if an NBA team should become interested.</p><p>After being waived by the&nbsp;Lakers in January of 2018, it was expected the free agent would join a contending team for a playoff push. But Bogut said at the time he\'d stay in Australia and focus on returning in the 2018-19 season. He also tweeted that his wife\'s pregnancy with their second child has been deemed \"high-risk\" and she was&nbsp;not allowed to travel at the time.</p><p>Bogut played 24 games for the Lakers after signing in September of 2017, averaging 1.5 points and 3.3 rebounds while starting five games. He was acquired by Cleveland in the second half of last season, but he fractured his left leg less than a minute into his debut with the Cavaliers. He played seven years with the Bucks (2005-12) before being traded to Golden State&nbsp;<a href=\"https://www.nba.com/warriors/news/warriors_acquire_bogut_jackson.html\" target=\"_blank\" style=\"color: rgb(29, 66, 138);\">in the Monta Ellis deal</a>.</p><p><em>Information from The Associated Press was used in this report.</em></p>', '1551897122andrew-bogut-ball-iso-0306.jpg', NULL, 1, 0, 0, 1, 1, 'admin', '2019-03-06 17:32:02', '2019-03-06 17:32:38'),
(7, 1, 'Still got it!! Mike Tyson Showing Skills to his new movie cast - Mike Tyson Highlights', 'Boxing', '<p><br></p>', NULL, 'https://www.youtube.com/watch?v=pZr_GsfYTWI', 122, 0, 0, NULL, 0, 'admin', '2019-03-06 17:40:55', '2019-06-24 07:01:39'),
(8, 1, 'Countdown to golf\'s \'fifth major\'', 'Golf', '<p>The world\'s top players head to TPC Sawgrass this month for the Players Championship, with extended coverage of golf\'s unofficial \'fifth major\' live on&nbsp;<em>Sky Sports</em>.</p><p>2018 champion Webb Simpson has the chance to become the first player in tournament history to successfully defend their title, following his four-shot victory last time around, while recent winners Rickie Fowler and Jason Day will also be among those in action.</p><p><span style=\"color: rgb(0, 0, 0);\">Two-time Players champion Tiger Woods is expected to return to action, having missed the Arnold Palmer Invitational with a neck injury, while Justin Rose and Rory McIlroy will be among the players bidding to become the first British winner since Sandy Lyle in 1987.</span></p>', '1551898133skysports-webb-simpson-golf_4597824.jpg', NULL, 118, 0, 0, 1, 0, 'admin', '2019-03-06 17:48:53', '2019-06-23 19:10:19'),
(9, 1, 'Cristiano Ronaldo: Juventus signed me for Champions League heroicsP', 'Football', '<h2>Ronaldo\'s treble brought Juventus back from a 2-0 aggregate deficit against Atletico Madrid</h2><p><br></p><h2><span style=\"color: rgb(74, 74, 74); font-weight: normal;\">Cristiano Ronaldo suggested Juventus brought him to the club \"to help do things they have never done before\" after his hat-trick heroics sent them to the Champions League last eight.</span></h2><p><br></p><h2><span style=\"color: rgb(74, 74, 74); font-weight: normal;\">Five-time Champions League winner Ronaldo has scored eight hat-tricks in the competition, the joint-most in the competition alongside Lionel Messi, and the Portuguese forward suggested his big-game impact is why Juventus brought him to the club from Real Madrid in the summer, with the Italians looking for a first European Cup crown since 1996.</span></h2><p><br></p><p>\"This was why Juventus brought me here, to help do things that they have never done before,\" said Ronaldo, now 34. \"This is the mentality you need to win in the Champions League.</p><p>\"I just do my work and I\'m very happy tonight, it was a magical night. Atletico are always difficult to face, but we are also strong and we proved it.</p><p><br></p>', '1552453751CC8936F9-D992-4130-8415-0BF6C3F9C85C.jpeg', NULL, 112, 0, 1, 1, 0, 'admin', '2019-03-13 04:09:11', '2019-06-24 09:12:57'),
(10, 1, 'Pep Guardiola wants Bayern Munich to beat Liverpool in Champions League', 'Football', '<h2>Guardiola speaks after Man City demolish Schalke 10-2 on aggregate to reach quarter-finals</h2><p><br></p><p>Pep Guardiola hopes former club Bayern Munich beat Liverpool and join Manchester City in the Champions League quarter-finals.</p><p><br></p><p>Schalke were ruthlessly swept aside by City at the Etihad as an emphatic seven-goal haul saw them cruise into the last eight courtesy of a 10-2 aggregate victory.</p><p><br></p><p>City\'s progression means there will be at least three English teams in the quarter-final of a Champions League campaign for the first time since 2010/11.</p><p>Liverpool will look to make that four by navigating Wednesday\'s last-16 second leg at the Allianz Arena, but Guardiola hopes City\'s title rivals stumble at his old club.</p><p>\"I\'m sorry for the English people but I want Bayern to go through,\" Guardiola said. \"I am part of this club. I love Munich. I love Bayern. I have a lot of friends there.\"</p><p><br></p><p>City\'s 10-2 aggregate win over Schalke was the biggest-ever by an English side in a Champions League knockout tie.</p><p>The emphatic victory was made all the more impressive by City coping with just one recognised central defender, with Vincent Kompany, Nicolas Otamendi and John Stones out injured.</p><p><br></p><p>\"We started the first 20-25 minutes trying not to lose what we achieved in Germany, that\'s why we didn\'t attack, and Leroy [Sane] and Raheem [Sterling] had no intention of being aggressive towards their full-backs.</p><p>\"But we didn\'t lose what we achieved and when we scored the first and the second goals qualification was almost done.</p><p>\"After that we played in a good level. We didn\'t concede chances, even in the last 15 minutes of the first half, and in the second half we were good in the transition and scored seven goals.</p><p><br></p><p>\"I know that Schalke are in a tough period because they lost the last game and their confidence was low, but even with that situation you have to do your job and we did it.</p><p>\"Last week we saw what happened with good results in the second leg, so we were serious, we did it and I\'m incredibly happy to be in Friday\'s draw.\"</p><p><br></p>', '155245428330D8E87C-2DE6-468A-AFCC-A87C67E9D25B.jpeg', NULL, 111, 0, 0, 1, 0, 'admin', '2019-03-13 04:18:03', '2019-06-23 14:10:45'),
(11, 7, 'Its Messi again for Barcelona', 'Football', '<p>After a quiet first leg it was no surprise to see Messi take control in the second.</p><p>The 31-year-old often stood still in the United half but would burst into life with devastating effect.</p><p>Messi\'s double took his goals tally to 45 in 42 games this term and made him the outright top scorer in this season\'s Champions League.</p><p>It was yet more success for the Argentine at United\'s expense, having also scored against them in both the 2009 and 2011 Champions League finals.</p><p>The home fans chanted Messi\'s name again and again during the game, but his performance was not just about his goals.</p><p>He amazed the jubilant fans in first-half stoppage time when turning Phil Jones on the halfway line, driving towards the area, and beating Jones twice more before feeding Alba down the left. Alba then crossed for Sergi Roberto but De Gea blocked the Spaniard\'s close-range shot on the goal line.</p><p>In such sublime form, Messi looks intent on leading his side to a first Champions League title since 2015.</p>', '1555505452_106479944_messi_getty4.jpg', NULL, 78, 0, 0, 0, 0, 'TORO', '2019-04-17 11:50:52', '2019-06-24 03:10:05'),
(12, 9, 'Super falcon', 'Football', '<p>I am super excited!!! Our girls are really making us proud @ super falcons</p>', NULL, NULL, 17, 0, 1, NULL, 0, 'esther okolo', '2019-06-24 03:07:21', '2019-06-24 11:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `posttags`
--

CREATE TABLE `posttags` (
  `id` int(10) UNSIGNED NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subtag` text COLLATE utf8mb4_unicode_ci,
  `view_count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posttags`
--

INSERT INTO `posttags` (`id`, `tag`, `subtag`, `view_count`, `created_at`, `updated_at`) VALUES
(1, 'Football', NULL, 143, '2018-11-27 21:52:35', '2019-06-24 02:48:31'),
(2, 'Table Tennis', NULL, 88, '2018-11-27 21:52:35', '2019-06-23 09:14:23'),
(3, 'Basketball', NULL, 73, '2018-11-27 21:52:35', '2019-06-23 17:59:11'),
(4, 'Boxing', NULL, 58, '2018-11-27 21:52:36', '2019-06-23 13:34:29'),
(5, 'Wrestling', NULL, 1, '2018-11-27 21:52:36', '2019-02-17 19:37:17'),
(6, 'Golf', NULL, 54, '2018-11-27 21:52:36', '2019-06-23 13:34:29'),
(7, 'F1', NULL, 0, '2018-11-27 21:52:36', '2018-11-27 21:52:36'),
(8, 'Olympics', NULL, 0, '2018-11-27 21:52:37', '2018-11-27 21:52:37'),
(9, 'Lawn Tennis', NULL, 0, '2018-11-27 21:52:37', '2018-11-27 21:52:37'),
(10, 'Rugby', NULL, 0, '2018-11-27 21:52:37', '2018-11-27 21:52:37'),
(11, 'Cricket', NULL, 0, '2018-11-27 21:52:38', '2018-11-27 21:52:38'),
(12, 'Swimming', NULL, 0, '2018-11-27 21:52:38', '2018-11-27 21:52:38');

-- --------------------------------------------------------

--
-- Table structure for table `subcomments`
--

CREATE TABLE `subcomments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment_user_id` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `privacy_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phonenumber` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateofbirth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_tag_interests` text COLLATE utf8mb4_unicode_ci,
  `social_media` text COLLATE utf8mb4_unicode_ci,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `isAdmin`, `name`, `email`, `firstname`, `lastname`, `phonenumber`, `dateofbirth`, `nationality`, `post_tag_interests`, `social_media`, `avatar`, `provider`, `status`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@spotrum.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '$2y$10$KYsO1a0Adkyt2v69Ntt/7OUg1cQc/kiT86erwVjGjScSk6o/UXz.2', 'WVScq8Vv5xoz9cc5qKd8hQnMJ4VMhuyltAcGzJwKjiufgQcMxDaqiy52YjjF', '2019-03-06 14:38:48', '2019-03-06 14:38:48'),
(2, NULL, 'Nelly Tadi', 'nellytadi@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'google', 0, '$2y$10$mKZd6yqlsKuBMGNc/xC7ZeYYcb0ty3hzuG0ezJIycNPIjUNXOVkvi', 'P3Re3EujvOwk2yPOVx8Woq16RzS1qvyP0baR0lMQrHiP2d89V4wmSjknVhPW', '2019-03-07 00:10:34', '2019-03-07 00:10:34'),
(3, NULL, 'Nathan Dauda', 'nathandauda27@gmail.com', NULL, NULL, NULL, NULL, 'Congo', '{\"post_tag_interests\":null}', '{\"facebook\":null,\"twitter\":null,\"instagram\":null}', '155192621723130974_878054899039351_5538782461020490229_n.jpg', 'google', 0, '$2y$10$g5oS09T5If2w8cSpyk/F1.07FfOVwPyUHLLGqJUAWTnPrLtwFQOFq', 'RDVoIIG1EOcBrSJtXwUBRD5TyD7dZr6GxVxDvoE2ZgzsfjEqyaVnUnNOOOrT', '2019-03-07 01:35:06', '2019-03-07 01:36:57'),
(4, NULL, 'Spotrum Online', 'spotrum@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'google', 0, '$2y$10$.YLXOeC6m7o9dxJ8mgz0ju8wmAeSMw/jVVcoOc8sWnffqQEpJ6/3C', 'NDLAQ5WxHVKHBXmiVPIlaDdveBYn5Q6za3neFZAqI48GVYQMMItVSQvYVJF9', '2019-03-13 20:33:34', '2019-03-13 20:33:34'),
(5, NULL, 'rickiedominic', 'rickiedominic@yahoo.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '$2y$10$2sMF.YLuLgR3hbNYgL/8C.TEbe6xRqGUaiGS7kcxyk2hC/2y6wyky', NULL, '2019-03-20 12:17:47', '2019-03-20 12:17:47'),
(6, NULL, 'dezukanma', 'dezukanma@hotmail.com', 'Divine', 'Ezukanma', '08027573650', '1974-01-07', 'Nigeria', '{\"post_tag_interests\":null}', '{\"facebook\":null,\"twitter\":null,\"instagram\":null}', '1553902546‪+234 802 757 3650‬ 20180802_222422.jpg', NULL, 0, '$2y$10$.u6DAZpUzq2RTwfNJ3WnleGaTdMYuxzcfa1M.sc9vx4eQidclbbcG', NULL, '2019-03-29 22:29:30', '2019-03-29 22:35:46'),
(7, NULL, 'TORO', 'toluolayinka@gmail.com', NULL, NULL, NULL, NULL, 'Nigeria', '{\"post_tag_interests\":[\"F1\",\"Swimming\"]}', '{\"facebook\":null,\"twitter\":null,\"instagram\":null}', '1554659273how-to-draw-rallo-tubbs-from-the-cleveland-show_1_000000003151_5.jpg', 'google', 0, '$2y$10$7Zbc4RFWfTO69FTJ8p1QjOPZhldkl.K3VjeqnKpKYwDp0tuAkCpUW', 'OsCvHrTO4A0hliwpQACMEkRSqcF6iTJzq1UvZl9d4KvGcIfKhE6UBEnOYqkc', '2019-04-07 16:46:09', '2019-04-07 16:47:53'),
(8, NULL, 'Marcus Aken', 'marcusaken@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'google', 0, '$2y$10$1KQJqCf9Gkw2exzdLxu7hO9bxA2b63H.DrPjK0Qm3PLAMm6ZiWnG.', 'osyx0Wbt49CBvj7QhWjp1ZKJdB2NPYbt96166YlFovnpcWTrmMmy7h6yZriw', '2019-05-08 09:25:50', '2019-05-08 09:25:50'),
(9, NULL, 'esther okolo', 'okoloesthermary@gmail.com', 'ESTHER', 'MARY', '8032231524', '1989-03-27', 'Nigeria', '{\"post_tag_interests\":[\"Football\",\"Basketball\",\"Wrestling\",\"Olympics\"]}', '{\"facebook\":null,\"twitter\":null,\"instagram\":null}', '1561347993MAKEUP_20190614142644_save.jpg', 'google', 0, '$2y$10$4n.pnIeTJSkizw3VWSFOr.d1JRMC4YbbHjh2ud8rcqr0bFXHBz4cy', 'Zjdu7VZvwAzgUmBtvnPx7k3DPjAiH0VCtCMseWcaakBpkuXYWjD84I3SB2QU', '2019-06-24 02:37:20', '2019-06-24 02:46:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `newsletters_email_unique` (`email`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posttags`
--
ALTER TABLE `posttags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcomments`
--
ALTER TABLE `subcomments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `posttags`
--
ALTER TABLE `posttags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `subcomments`
--
ALTER TABLE `subcomments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
