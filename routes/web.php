<?php
use App\Posttag;
use App\Notification;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index')->name('index');

Route::get('/admin/register', 'Auth\RegisterController@showAdminRegistrationForm');
Route::get('/post/{id}/{posttitle}', 'IndexController@show')->name('post.show');
Route::get('/post/{id}/{posttitle}/like', 'LikeController@show')->name('post.like.show');
Route::get('login/{social}', 'Auth\LoginController@redirectToProvider');
Route::get('/{social}/callback', 'Auth\LoginController@handleProviderCallback');

Auth::routes();

Route::get('change/password','SecurityController@show')->name('change.password');
Route::post('change/password/submit','SecurityController@submit')->name('change.password.submit');
Route::get('/home', 'HomeController@index')->name('home');

Route::post('post/submit','PostController@store')->name('post.submit');
Route::get('post/comment','CommentController@store')->name('post.comment');

Route::get('post/subcomment','CommentController@storeSubcomment')->name('post.subcomment');

Route::get('posts/created','PostController@viewPosts')->name('posts.created');

Route::get('post/delete/now/{id}/','PostController@delete')->name('post.delete');
Route::get('post/edit/now/{id}','PostController@edit')->name('post.edit');
Route::post('post/update/submit','PostController@update')->name('post.update.submit');
Route::get('search/{tag}','IndexController@search')->name('search');
Route::get('/search/tag','IndexController@search')->name('search');
Route::get('/profile','ProfileController@show')->name('profile');
Route::post('profile/update','ProfileController@update')->name('profile.update');

Route::get('delete/comment/{id}','CommentController@deletecomment')->name('delete.comment');
Route::get('delete/subcomment/{id}','CommentController@deletesubcomment')->name('delete.subcomment');

Route::get('all/posts/created','PostController@viewAllPosts')->middleware('admin');
Route::get('admin/routes', 'HomeController@admin')->middleware('admin');
Route::get('/create/ads','AdsController@show')->middleware('admin');
Route::post('/store/ads','AdsController@store')->middleware('admin');
Route::get('all/users','SecurityController@viewusers')->name('view.users')->middleware('admin');
Route::get('user/profile/{id}','SecurityController@showuser')->middleware('admin');
Route::get('delete/user/{id}','SecurityController@deleteuser')->middleware('admin');

Route::get('/unsubscribe','NewsletterController@edit');
Route::post('/unsubscribe','NewsletterController@delete')->name('unsubscribe');

Route::get('/privacy-policy', function(){
	$posttags=Posttag::orderBy('view_count','desc')->get();
	if(Auth::check()){
    $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
	    
	}
    return view('privacy-policy',compact('posttags','notifications'));
});
Route::get('/terms-and-conditions', function(){
	$posttags=Posttag::orderBy('view_count','desc')->get();
   if(Auth::check()){
    $notifications=Notification::where('status',0)->where('post_user_id',Auth::user()->id)->limit(10)->get();
	    
	}
    return view('terms-and-conditions',compact('posttags','notifications'));
});
