@extends('layouts.app')
@section('body')
<style type="text/css">
	.icon-et i:hover{
		color: #3C5A99;
		cursor: pointer;
	}
	.icon-et i{
		color:#b3b3b3;
		padding-right: 10px;
	}
</style>
<!-- post wraper start-->
<section class="block-wrapper mt-15 category-layout-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="ts-grid-box">
                    <ol class="ts-breadcrumb">
                        <li>
                            <a href="{{url('/')}}">
                                <i class="fa fa-home"></i>
                                Home

                            </a>
                        </li>
                        

                    </ol>
                    <div class="clearfix entry-cat-header">
                        <h2 class="ts-title float-left">All Posts</h2>
                    </div>

                </div>
                <!-- ts-populer-post-box end-->
                <div class="row">
                	@foreach($posts as $post)
                    <div class="col-lg-4 col-md-6">
                        <div class="ts-grid-box ts-grid-content">
                            <a class="post-cat ts-orange-bg" href="#">{{$post->posttags}}</a>
                             <div class="ts-post-thumb">
                                        <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}">
                                            @if($post->youtube_video)
                                            <img class="img-fluid" src="{{asset('images/banner/youtube-logo.jpg')}}" alt="">
                                            @elseif($post->postcoverimage)
                                            <img class="img-fluid" src="{{asset('post/thumbnail/'.$post->postcoverimage)}}" alt="">
                                            @else
                                            <img class="img-fluid" src="{{asset('images/banner/thumbnails.jpg')}}" alt="">
                                            @endif
                                        </a>
                                    </div>
                            <div class="post-content">
                                <h3 class="post-title">
                                    <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}">{{$post->posttitle}}</a>
                                </h3>
                                <span class="post-date-info">
										<i class="fa fa-clock-o"></i>
										<?php echo date('M d, Y',strtotime($post->created_at));?>
										<span class="icon-et" style="float: right; font-size: 16px; font-weight: 600;margin-right: 10px; ">
											
										<a href="{{url('post/edit/now/'.$post->id)}}"><i class="fa fa-edit"></i></a>
									
										<a onclick="if(!confirm('Are you sure you want to delete this post?')){return false;}" href="{{url('/post/delete/now/'.$post->id)}}"><i class="fa fa-trash"></i></a>
										</span>
									</span>
                            </div>
                        </div>
                        <!-- ts grid box-->
                    </div>
                    @endforeach
                    <!-- col end-->
                   
                </div>
            
                
                <!-- row-->
                <!-- pagination-->
                <div class="ts-pagination text-center mb-20">
                	{{ $posts->links() }}
                   
                </div>
                <!-- pagination end-->
            </div>
          
            <div class="col-lg-3">
                <div class="right-sidebar">

                    <!-- Login Model here-->
                    <div id="side-twitter">
                    <a class="twitter-timeline" href="https://twitter.com/Spotrum4" data-chrome="nofooter" > Tweets by @Spotrum4 </a>
                </div>


                @if(isset($ads))

                    <div class="widgets widget-banner">
                       <a href="{!!$ads->ad_link!!}" title="{{$ads->ad_owner}}">
                        <img class="img-fluid" src="{{asset('ads/'.$ads->ad_image)}}" alt="Ad">
                        </a>

                    </div>
                    @else
                    <div class="widgets widget-banner">
                        
                        <a data-toggle="modal" data-target="#bd-example-modal" href="#">
                               <img class="img-fluid" src="{{asset('images/banner/Ads-holder.jpg')}}" alt="">
                           </a>

                    </div>
                  @endif
                      <!-- widgets end-->
                    <div class="post-list-item widgets">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation">
                                <a class="active" href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                    <i class="fa fa-clock-o"></i>
                                    Popular tags
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                                    <i class="fa fa-heart"></i>
                                    Popular Posts
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active ts-grid-box post-tab-list" id="home">
                                  <?php
                            $i=0;
                            foreach($posttags as $tag) { 
                               echo '<div class="post-content media">
                                   
                                    <div class="media-body">
                                            <span class="post-tag" >
                                                 <a style="color:#000;" href="/search/'.$tag->tag.'" >'.$tag->tag.'</a>
                                            </span>
                                        
                                    </div>
                                </div>';
                               $i++;
                               if ($i>=6) {
                                  break;
                               }
                           }
                           ?>
                                

                            </div>
                            <!--ts-grid-box end -->

                            <div role="tabpanel" class="tab-pane ts-grid-box post-tab-list" id="profile">
                                @foreach( $popularposts as $post)
                                <div class="post-content media">
                                   
                                    <div class="media-body">
                                            <span class="post-tag">
                                                <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}"> {{$post->posttags}}</a>
                                            </span>
                                        <h4 class="post-title">
                                            <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}">{{str_limit($post->posttitle,65)}} </a>
                                        </h4>
                                    </div>
                                </div>
                               @endforeach
                            </div>
                            <!--ts-grid-box end -->
                        </div>
                        <!-- tab content end-->

                    </div>

              @if(!Auth::check())

                    <div class="widgets widget-banner">
                        
                        <a data-toggle="modal" data-target="#bd-example-modal" href="#">
                               <img class="img-fluid" src="{{asset('images/banner/sidebar-banner1.jpg')}}" alt="">
                        </a>

                    </div>
                    @else
                    <div class="widgets widget-banner">
                        
                        <a href="mailto:contact@spotrum.com?Subject=Request to place ads">
                               <img class="img-fluid" src="{{asset('images/banner/Ads-holder.jpg')}}" alt="">
                           </a>
                    </div>
                    @endif
                    <!-- widgets end-->

                  
        </div>
        <!-- row end-->
    </div>
    <!-- container end-->
</section>
<!-- post wraper end-->

@endsection