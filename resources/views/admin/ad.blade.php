@extends('layouts.app')

@section('body')
<div class="container" >
    <div class="row">
      <div class="col-md-3" style="padding: 20px;background: #fff;margin:20px 0px;">
      <ul class="nav flex-column">
 <li class="nav-item">
    <a class="nav-link" href="{{url('/')}}">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('create/ads')}}">Create new ads <i class="fa fa-plus"></i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('posts/created')}}">My posts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/posts/created')}}">View all posts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/users')}}">View users</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{route('change.password')}}">Security and privacy</a>
  </li>
</ul>
</div>
  
        <div class="col-md-9" style="padding: 20px;background: #fff;margin:20px 0px;">
              <h5 class="heading-design-h5">Create Web Advert </h5>
               @foreach ($errors->all() as $message) 
               {{$message}}
               @endforeach
            <form action="{{url('store/ads')}}" method="POST" enctype="multipart/form-data"  onsubmit="return checkForm()">
                {{csrf_field()}}
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-6">

                       <div class="blog_img" style="position: relative;text-align: center;">
                      <label>Selet cover image for blog post</label><br>
                                <img src="{{asset('images/banner/ad.jpg')}}" id="preview" class="img-fluid" />
                               
                                  <input type="file" name="ad_image" accept="image/*" id="image_file" onchange="fileSelectHandler()" title="select cover picture for blog post" required>
                                  <div class="error"></div>
                          
                               
                      </div>
                    </div>
                    <div class="col-md-6">
                            
                        <label>Website link</label>
                        <input type="url" name="ad_link" placeholder="Website url..." class="form-control" required>
                        <br>
                        <label>Company/Website/Owner's Name/</label>
                        <input type="text" name="ad_owner" placeholder="Name..." class="form-control" required>
                        <br>
                        <button class="btn btn-lg btn-theme-round btn-block">Submit</button>
                    </div>
                  </div>
                </div>
                 
                   
              </form>
        </div>
    </div>
</div>

@endsection
