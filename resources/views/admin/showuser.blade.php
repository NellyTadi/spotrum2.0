@extends('layouts.app')

@section('body')
<style type="text/css">
  .shift{
    color: #000;
    font-weight: 600;

  }
  .social-list .facebook{
    color: #3C5A99;
 }
 .social-list .instagram{
    color: #E1306C;
 }
  .social-list .twitter{
    color: #1DA1F2;
 }
</style>
<div class="container" >
    <div class="row">
      <div class="col-md-3" style="padding: 20px;background: #fff;margin:20px 0px;">
      <ul class="nav flex-column">
<li class="nav-item">
    <a class="nav-link" href="{{url('/')}}">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('create/ads')}}">Create new ads <i class="fa fa-plus"></i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('posts/created')}}">My posts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/posts/created')}}">View all posts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/users')}}">View users</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{route('change.password')}}">Security and privacy</a>
  </li>
</ul>
</div>
  <div class="col-md-9" style="padding: 20px;background: #fff;margin:20px 0px;">
              <h5 class="heading-design-h5">Profile</h5>
                {{csrf_field()}}
                <div class="row">
                <div class="col-md-4">
                    <div style="max-height: 300px; overflow: hidden;">
                @if($user->avatar == '')
                <img src="{{asset('images/icon/human.png')}}" class="img-fluid" id="avatar" alt="profile picture" >
                @else
                <img src="{{asset($user->avatar)}}" class="img-fluid" id="avatar" alt="profile picture">
                @endif
                </div>
               
            </div><br>

            <div class="col-md-8">
               <div class="row">
                <div class="col-md-6">
                  <label>Username</label>
                  <div class="shift"> 
                    {{$user->name}} 
                  </div>
              </div>
              <div class="col-md-6">
            <label>Email</label>
                <div class="shift"> 
                  {{$user->email}}
                </div>
              </div>
            </div>
              <div class="row">
                <div class="col-md-6">
                <label>First name</label>
                  <div class="shift">
                  {{$user->firstname}}
                </div>
               </div>
              <div class="col-md-6">
                <label>Last name</label>
                <div class="shift">
                {{$user->lastname}}
              </div>
              </div>
            </div>
           <div class="row">
                <div class="col-md-6">
              <label>Phone number</label>
                <div class="shift">
                  {{$user->phonenumber}}
                </div>
             </div>
             
            <div class="col-md-6">
                <label>Date of birth</label>
                <div class="shift">
                  {{$user->dateofbirth}}
                </div>
              
              </div>
              </div>
              <div class="row">
              <div class="col-md-6">
                <label>Nationality</label>
                <div class="shift">
                  {{$user->nationality}}
                </div>
              </div>
            
             <div class="col-md-6">
            <label>Interests & Categories</label>
                <div class="shift">
                  
                        @if($user->post_tag_interests != '')
                        <?php
                        $post_tag=json_decode($user->post_tag_interests,true);
                        if ($post_tag['post_tag_interests']!='') {
                          foreach ($post_tag['post_tag_interests'] as $key ) {
                           echo $key.', ';
                          }
                           
                         }
                           ?>
                           @endif
                </div>
              </div>
            </div>
              <br>
            <label>Linked Accounts</label>
               <div class="shift"> 
                <ul class="social-list">
                <?php
                $social_media=json_decode($user->social_media,true);
                echo '<li class="facebook" id="facebookss" title="facebook" ><i class="fa fa-facebook"></i> 
                    '.$social_media['facebook'].'</li><br>
                <li class="instagram" id="instagramss" title="instagram" ><i class="fa fa-instagram"></i> 
                    '.$social_media['instagram'].'</li><br>
                <li class="twitter" id="twitterss" title="twitter"><i class="fa fa-twitter"></i> 
                   '.$social_media['twitter'].'
                </div></li><br>';
                ?>
                </ul>
            
          </div>
        </div>
            <a onclick="if(!confirm('Are you sure you want to delete this user?')){return false;}" href="{{url('delete/user/'.$user->id)}}"><button  class="btn btn-theme-round" style="float: right;">Delete user</button></a>
        </div>
        </div>
        
        </div>
    </div>
</div>


@endsection