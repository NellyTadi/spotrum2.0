@extends('layouts.app')

@section('body')
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />-->
 <style type="text/css">

.select2-selection__choice__remove {
    display: none !important;
}

 </style>
 <link rel="stylesheet" href="{{asset('js/jcrop/css/jquery.Jcrop.css')}}" type="text/css" />
<div class="container" >
    <div class="row">
      <div class="col-md-3" style="padding: 20px;background: #fff;margin:20px 0px;">
      <ul class="nav flex-column">
<li class="nav-item">
    <a class="nav-link" href="{{url('/')}}">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('create/ads')}}">Create new ads <i class="fa fa-plus"></i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('posts/created')}}">My posts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/posts/created')}}">View all posts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/users')}}">View users</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{route('change.password')}}">Security and privacy</a>
  </li>
</ul>
</div>
  
        <div class="col-md-9" style="padding: 20px;background: #fff;margin:20px 0px;">
              <h5 class="heading-design-h5">Admin Create blog post</h5>
               @foreach ($errors->all() as $message) 
               {{$message}}
               @endforeach
            <form action="{{url('post/submit')}}" method="POST" id="post-form" enctype="multipart/form-data"  onsubmit="return checkForm()">
                {{csrf_field()}}
    
 <div class="blog_img" style="position: relative;text-align: center;">
                  <label>Selet cover image for blog post</label><br>
                            <img src="{{asset('images/banner/temp.jpg')}}" id="preview" class="img-fluid" />
                           
                              <input type="file" name="postcoverimage" accept="image/*" id="image_file" onchange="fileSelectHandler()" title="select cover picture for blog post">
                              <div class="error"></div>
                              <input type="hidden" id="x1" name="x1" />
                              <input type="hidden" id="y1" name="y1" />
                              <input type="hidden" id="x2" name="x2" />
                              <input type="hidden" id="y2" name="y2" />
                              <input type="hidden" id="w" name="w" />
                              <input type="hidden" id="h" name="h" />
                              <input type="hidden" id="filesize" name="filesize" />
                             <input type="hidden" id="filetype" name="filetype" />
                              <input type="hidden" id="filedim" name="filedim" />
                           
                  </div>
                <label>Post Title</label>
                <input type="text" name="posttitle" class="form-control" required placeholder="Post Title"><br>
              
<label>Select tag relating to post</label>
    <select  required name="posttags" class="form-control">
        <option disabled></option>
        @foreach($posttags as $tag)
        <option value="{{$tag->tag}}"> {{$tag->tag}} </option>
        @endforeach
    </select>
<br>
<label>Add Youtube Video Link</label>
<input type="url" name="youtube_video" class="form-control" placeholder="* optional- enter URL to youtube video" >
<input type="hidden" name="postbody" required>
<label>Post</label>
                <div id="editor">
                    
                </div><br>
                <button class="btn btn-lg btn-theme-round btn-block">Publish</button>
            </form>
        </div>
    </div>
</div>

@endsection
