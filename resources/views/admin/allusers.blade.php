@extends('layouts.app')

@section('body')

<div class="container" >
    <div class="row">
      <div class="col-md-3" style="padding: 20px;background: #fff;margin:20px 0px;">
      <ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link" href="{{url('/')}}">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('create/ads')}}">Create new ads <i class="fa fa-plus"></i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('posts/created')}}">My posts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/posts/created')}}">View all posts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/users')}}">View users</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{route('change.password')}}">Security and privacy</a>
  </li>
</ul>
</div>
  <div class="col-md-9" style="padding: 20px;background: #fff;margin:20px 0px; overflow-y: auto; max-height: 800px;">
    <table class="table table-sm table-striped" ">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Username</th>
      <th scope="col">First name</th>
      <th scope="col">Last name</th>
      <th scope="col">Email</th>
      <th scope="col"></th>
     
    </tr>
  </thead>
  <tbody style="color: #000;">
    @foreach($users as $user)
    <tr>
      <th scope="row">{{$user->id}}</th>
      <td>{{$user->name}}</td>
      <td>{{$user->firstname}}</td>
      <td>{{$user->lastname}}</td>
      <td>{{$user->email}}</td>
      <td><a href="{{url('user/profile/'.$user->id)}}"><button class="btn  btn-theme-round btn-block">edit</button></a></td>
    </tr>
    @endforeach
    
  </tbody>
</table>
  </div>
</div>
</div>
@endsection