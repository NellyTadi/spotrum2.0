@extends('layouts.app')

@section('body')
<style type="text/css">
   @media screen and (max-width: 992px) {
        .login-logo{
            display: none;
        }
    }
</style>
<div class="container">
   
        <div class="col-lg-12" style="background: #fff; margin: 50px 0px; padding: 30px;">
              <div class="row">
        <div class="col-lg-6 login-logo">
          <img src="{{asset('images/login_register_bg.png')}}">
        </div>
        <div class="col-lg-6">
            <div class="panel panel-default">
               <h5 class="heading-design-h5">Unsubscribe</h5>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('unsubscribe') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Enter E-Mail</label>

                            <div class="col-md-10">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"  placeholder="johndoe@example.com" required>

                                @if ($errors->has('email'))
                                    <span class="help-block" >
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group col-md-10">
        
                                            <fieldset class="form-group">
                                                <button type="submit" class="btn btn-lg btn-theme-round btn-block">
                                                    Unsubscribe
                                                </button>
                                            </fieldset>
                                        </div>

                                           
                    </form>
                   
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
