@extends('layouts.app')

@section('body')
<style type="text/css">
 @media screen and (max-width: 992px) {
        .login-logo{
            display: none;
        }
    }
</style>
<div class="container">
   
        <div class="col-lg-12" style="background: #fff; margin: 50px 0px; padding: 30px;">
              <div class="row">
        <div class="col-lg-6 login-logo">
          <img src="{{asset('images/login_register_bg.png')}}">
        </div>
        <div class="col-md-6 ">
           
                <h5 class="heading-design-h5">Login to your account</h5>

                
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Enter E-Mail</label>

                            <div class="col-md-10">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Enter Password</label>

                            <div class="col-md-10">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-4">
                                  <button type="submit" class="btn btn-lg btn-theme-round btn-block">Log in
                                                    to your account
                                                </button>
                                <div style="text-align: center;">
                                <a  href="{{ route('password.request') }}" >
                                    Forgot Your Password?
                                </a>
                                </div>
                            </div>
                        </div>
                    </form>

                
         
            <div class=" col-lg-10 login-with-sites text-center">
                                                <p>or Login with your social profile:</p>
                    <a href="{{url('login/facebook')}}">
                        <button class="btn-facebook login-icons btn-lg">
                            <i class="fa fa-facebook"></i> Facebook
                        </button>
                    </a>
                    <a href="{{url('login/google')}}">
                        <button class="btn-google login-icons btn-lg">
                            <i class="fa fa-google"></i> Google
                        </button>
                    </a>
                                           
                                           <hr>
                                           
                                           <a href="{{route('register')}}">Don't have an account? Sign up here</a>
                                        </div>
      </div>
    </div>
    </div>
</div>
@endsection
