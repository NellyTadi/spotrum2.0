@extends('layouts.app')

@section('body')

<div class="container" >
    
    <div class="row">
         <div class="col-md-3" style="padding: 20px;background: #fff;margin:20px 0px;">
      <ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link" href="{{url('/')}}">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('create/ads')}}">Create new ads <i class="fa fa-plus"></i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('posts/created')}}">My posts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/posts/created')}}">View all posts</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{route('change.password')}}">Security and privacy</a>
  </li>
</ul>
</div>

       
        <div class="col-md-9" style="padding: 20px;background: #fff;margin:20px 0px;">
              <h5 class="heading-design-h5">Change Password</h5>
                @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                     @if (session('error'))
                        <div class="alert alert-warning">
                            {{ session('error') }}
                        </div>
                    @endif
                    @foreach ($errors->all() as $message) 
                    <div class="alert alert-warning">
				       {{$message}}
				   </div>
       @endforeach
            	<form action="{{route('change.password.submit')}}" method="POST"> 
            		{{csrf_field()}}
            		<label><b>Current Password</b></label>
            			<input type="password" name="current-password" class="form-control" placeholder="*********" required><br>
            			<div class="row">
            			<div class="col-md-6">
            		<label><b>New Password</b></label>
            			<input type="password" name="new-password" class="form-control" placeholder="*********" required>

            			</div>
            			<div class="col-md-6">
            		<label><b>Confirm New Password</b></label>
            			<input id="password-confirm" type="password" class="form-control" placeholder="********" name="new-password_confirmation" required>
            		</div>
            	</div>
            			<br>
            			 <button class="btn btn-lg btn-theme-round btn-block">Submit</button>
            	</form>
        </div>
    </div>
</div>

@endsection