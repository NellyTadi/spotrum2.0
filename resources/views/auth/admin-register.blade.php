@extends('layouts.app')

@section('body')
<div class="container">
   
        <div class="col-lg-12" style="background: #fff; margin: 50px 0px; padding: 30px;">
              <div class="row">
        <div class="col-lg-6">
          <img src="{{asset('images/login_register_bg.png')}}">
        </div>
        <div class="col-lg-6">
            <div class="panel panel-default">
               <h5 class="heading-design-h5">Admin Register</h5>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Enter E-Mail</label>

                            <div class="col-md-10">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"  placeholder="johndoe@example.com" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Enter Password</label>

                            <div class="col-md-10">
                                <input id="password" type="password" class="form-control" name="password" placeholder="********" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <input type="hidden" name="isAdmin" value="1">
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-10">
                                <input id="password-confirm" type="password" class="form-control" placeholder="********" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group col-md-10">
                            <p>
                                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                                    <input type="checkbox" class="custom-control-input" required>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">I Agree with<a href="{{url('/terms-and-conditions')}}"> Term and Conditions </a> </span>
                                                </label>
                                            </p>
                                            <fieldset class="form-group">
                                                <button type="submit" class="btn btn-lg btn-theme-round btn-block">
                                                    Create Your Account
                                                </button>
                                            </fieldset>
                                        </div>

                                           
                    </form>
                    <div class=" col-lg-10 login-with-sites text-center">
                                           
                                           <a href="{{route('register')}}">Not an Admin. Click here to go to Register</a>
                                        </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
