@extends('layouts.app')

@section('body')

<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />-->
 <style type="text/css">

.select2-selection__choice__remove {
    display: none !important;
}
.side-list .active,.side-list li:hover{
   background: #0d1943;
   padding: 10px;
   color: #fff;
    transition: 0.3s;
 }   
 .side-list li{
    color: #424242;
    padding: 5px;
    margin: 5px;
 }
 .blog_img{
    opacity: 1;
     -webkit-transition: opacity 1s; /* Safari */
    transition: opacity 1s;
}
    .blog_img:hover{
      opacity: 0.6;
      -webkit-transition: opacity 1s; /* Safari */
    transition: opacity 1s;
    }
    .btn-round{
        border-radius: 20px;
        width:auto;
        background: rgba(0,0,0,0.5);
        padding: 10px 40px; 
        color: #fff;
        border: 1px solid #fff;
        text-align: center;
    }
   #postcover-img:hover, .btn-round:hover{
         background: rgba(0,0,0,0.9);
    }
  
 </style>

 
<div class="container" >
    
    <div class="row">
              <div class="col-md-3" style="padding: 20px;background: #fff;margin:20px 0px;">
      <ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link" href="{{url('/')}}">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('create/ads')}}">Create new ads <i class="fa fa-plus"></i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('posts/created')}}">My posts</a>
  </li>
  @if(Auth::user()->isAdmin == 1)
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/posts/created')}}">View all posts</a>
  </li>
    <li class="nav-item">
    <a class="nav-link " href="{{url('create/ads')}}">Create new ads <i class="fa fa-plus"></i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/users')}}">View users</a>
  </li>
  @endif
  <li class="nav-item">
    <a class="nav-link " href="{{route('change.password')}}">Security and privacy</a>
  </li>
</ul>
</div>
       
        <div class="col-md-9" style="padding: 20px;background: #fff;margin:20px 0px;">
              <h5 class="heading-design-h5">Change Password</h5>
                @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                     @if (session('error'))
                        <div class="alert alert-warning">
                            {{ session('error') }}
                        </div>
                    @endif
                    @foreach ($errors->all() as $message) 
                    <div class="alert alert-warning">
				       {{$message}}
				   </div>
       @endforeach
            	<form action="{{route('change.password.submit')}}" method="POST"> 
            		{{csrf_field()}}
            		<label><b>Current Password</b></label>
            			<input type="password" name="current-password" class="form-control" placeholder="*********" required><br>
            			<div class="row">
            			<div class="col-md-6">
            		<label><b>New Password</b></label>
            			<input type="password" name="new-password" class="form-control" placeholder="*********" required>

            			</div>
            			<div class="col-md-6">
            		<label><b>Confirm New Password</b></label>
            			<input id="password-confirm" type="password" class="form-control" placeholder="********" name="new-password_confirmation" required>
            		</div>
            	</div>
            			<br>
            			 <button class="btn btn-lg btn-theme-round btn-block">Submit</button>
            	</form>
        </div>
    </div>
</div>

@endsection