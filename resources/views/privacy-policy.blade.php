@extends('layouts.app')

@section('body')
<style>
    .dash-top{
    margin:20px 30px;
    background: #fff;
    padding: 15px 15px ;
    -webkit-box-shadow: 7px 6px 22px 2px rgba(230,230,230,1);
-moz-box-shadow: 7px 6px 22px 2px rgba(230,230,230,1);
box-shadow: 7px 6px 22px 2px rgba(230,230,230,1);
    height: auto;
  }
  p{
      padding:15px 25px;
  }
  h3{
      padding:10px;
  }
  h4{
      padding:10px 15px;
  }
  ul.list{ list-style-type: square;padding:0px 40px;color:black;}
</style>
<div class="container ">
    <div class="dash-top">
<h1 style="padding-left:20px;">Privacy Policy</h1>

<h3>What’s New in the Updated Privacy Policy?</h3>
    <p>We have updated the privacy policy applicable to our online and mobile websites, applications, and digital services and clarified data collection, usage and sharing practices among spotrum, its parent and affiliates and unaffiliated third parties. Please take the time to read the full privacy statement. You consent to the data collection, use, disclosure and storage practices described in this privacy policy when you use any of our Services (as described below), including when you access any content or videos.</p>
    
<h3>	The Information We Collect.</h3>
<p>We receive both information that is directly provided to us, such as personal information you provide when you visit the Services, and information that is passively or automatically collected from you, such as anonymous information collected from your browser or device. In this Privacy Policy, we refer to all of this as the “Information”.</p>
<h4>	Information You Provide To Us.</h4>
<p>At some Services, you can register, order products, enter contests, vote in polls or otherwise express an opinion, subscribe to one of our services such as our online newsletters or text message alerts, or participate in one of our online forums or communities. In the course of these various offerings, we often seek to collect from your various forms of information, such as: name, address, e-mail address, telephone number, fax number and credit card information.</p>
<p>At some Services, you may also be able to submit Information about other people. For example, you might submit a person's name and e-mail address to send an electronic greeting card and, if you order a gift online and want it sent directly to the recipient, you might submit the recipient's name and address. This Information may include, for instance, a recipient's name, address, e-mail address, and telephone number.</p>
<h4>	Information That is Passively or Automatically Collected.</h4>
<p>We, and our “Partners” who include Turner, Turner Affiliates, third party service providers, advertisers, advertising networks and platforms, agencies, and distribution or other partners may use automated means to collect various types of Information about you, your computer or other device used to access, or in connection with, our Services. A representative, non-exhaustive list of the types of automatically collected information may include: network or Internet protocol address and type of browser you are using (e.g., Chrome, Safari, Firefox, Internet Explorer), the type of operating system you are using, (e.g., Microsoft Windows or Mac OS), the name of your Internet service provider and domains used by such providers, mobile network, device identifiers (such as an Apple IDFA or an Android Advertising ID), device settings, device attributes, browser settings, the web pages of the Services you have visited, Services visited before and after you visit a Service, the type of handheld or mobile device used to view the Service (e.g., iOS, Android), location information, and the content and advertisements you have accessed, seen, forwarded and/or clicked on. Please see our Section IV titled Cookies, and Other User and Ad-Targeting Technologies for more information about how the foregoing Information may be collected and used.</p>
<h4>Geo-location Information.</h4> 
<p>If you are accessing a Service from a mobile device or through a mobile application, you may be asked to share your precise (GPS level) geo-location information with us so we can customize your experience on our Services or on other Services, when we work with a Partner such as a third party mobile ad platform. If you agree to such collection, in most cases, you will be able to turn off such data collection at any time by accessing the privacy settings of your mobile device and/or through the settings in the applicable mobile application.</p>

<h4>Information Collected By and From Social Media Services and Other Third Party Platforms.</h4>
<p>You also can engage with our content, such as video, games, applications, and other offerings, on or through social media services or other third party platforms, such as Facebook, or other third-party social media plug-ins, integrations and applications. When you engage with our content on or through social media services or other third party platforms, plug-ins, integrations or applications, you may allow us to have access to certain Information in your profile. This may include your name, e-mail address, photo, gender, birthday, location, an ID associated with the applicable third party platform or social media account user files, like photos and videos, your list of friends or connections, people you follow and/or who follow you, or your posts or “likes.” For a description on how social media services and other third party platforms, plug-ins, integrations or applications handle your information, please refer to their respective privacy policies and terms of use, which may permit you to modify your privacy settings.</p>
<p>When we interact with you through our content on third party websites, applications, integrations or platforms, we may obtain any information regarding your interaction with that content, such as content you have viewed, your game performance, high scores, and information about advertisements within the content you have been shown or may have clicked on.</p>

<h3>How We Use the Information</h3>
<p>We, along with our Partners, may use the Information to:</p>

    <ul class="list">
        <li>provide and communicate with you about the Services or your account with us,</li>
        <li>fulfill your requests regarding the Services, including without limitation requests for newsletters and notifications,</li>
        <li>respond to your inquiries, and notify you if you have won a contest,</li>
        <li>communicate with you about other products, programs or services that we believe may be of interest to you,</li>
        <li>enforce the legal terms (including without limitation our policies and terms of service) that govern your use of our Services, and/or for the purposes for which you provided the Information,</li>
        <li>provide technical support for the Services,</li>
        <li>prevent fraud or potentially illegal activities (including, without limitation, copyright infringement) on or through the Services,</li>
        <li>protect the safety of our Users,</li>
        <li>customize or personalize ads, offers and content made available to you based on your visits to and/or usage of the Services or other online or mobile websites, applications, platforms or services, and analyze the performance of those ads, offers and content, as well as your interaction with them,</li>
        <li>perform analysis regarding how you use the Services or any part thereof,</li>
        <li>send gifts, cards, invitations or emails if you use these services, to your designated recipients.</li>
    </ul>

<p>When you provide information from your social media account, it can help enable us to do things like (1) give you exclusive content, (2) personalize your online experience with us within and outside our Services, (3) contact you through the social media service or directly by sending you the latest news, special offerings, and rewards, and (4) enable you to share your experience and content via social media services. When you provide information about yourself to us through an application, through our Services, or on social media services or other third party platforms, it may be publicly viewed by other members of these services and we cannot prevent further use of the information by third parties.</p>
<p>We or a Partner may use “cookies” or similar technologies to associate certain of the Information with a unique identifier that then associates the Information with your device or browser. For information about how these technologies work and how we may use them, please go to Section IV, titled Cookies, And Other User And Ad-Targeting Technologies. Sometimes, we may associate cookies with the Information, including de-identified, “hashed,” or anonymous versions of Information you have provided (such as during registration), in order to send or help our Partners send ads and offers based on your presumed interests or demographic information.</p>
<p>We may combine any of the Information we collect, for any of the above purposes, and may sometimes enhance the Information with other information that we obtain from third party sources. Please also be aware that if you choose to submit Information or content for publication (e.g., a letter to our editors, comments sent to our television personalities, a posting to a blog or a discussion board, an article, or a video), we may publish that Information and content, along with other Information about you (such as your name, screen name or location). Likewise, if you register and create a profile with our Service, the screen name you select as well as other content you submit to your profile (e.g., photos, comments, video, reviews, posts, articles) will be displayed publicly on the Service, is searchable, can be found by third parties and may be reused and redistributed by us in our sole discretion. See our Terms of Use for details on our use of content you submit.</p>
<h4>	Information Sharing and Disclosure</h4>
<p>We may disclose the Information as follows:</p>
<ul class="list">
<li>To service providers or Partners that we have engaged to perform business-related functions on our behalf. This may include service providers that: (a) conduct research and analytics; (b) create content; (c) provide customer, technical or operational support; (d) conduct or support marketing (such as email or advertising platforms); (e) fulfill orders and user requests; (f) handle payments; (g) host our Services, forums and online communities; (h) administer contests; (i) maintain databases; (j) send or support online or mobile advertising; and (k) otherwise support our Services.</li>
<li>In response to legal process, for example, in response to a court order or a subpoena, a law enforcement or government agency's request or similar request.</li>
<li>With third parties in order to investigate, prevent, or take action (in our sole discretion) regarding potentially illegal activities, suspected fraud, situations involving potential threats to any person, us, or the Services, or violations of our policies, the law or our Terms of Use, to verify or enforce compliance with the policies governing our Services.</li>	
<li>We may transfer some or all of your Information if we, or one of our business units, undergoes a business transition, like a merger, acquisition by another company, or sale of all or part of our assets, or if a substantial portion of our or of a business unit’s assets is sold or merged in this way.</li>
<li>We may share the Information with Turner Affiliates, so they can provide, improve and communicate with you about their own, or their marketing partners’ products and services. </li>
<li>We may share the Information with unaffiliated Partners and third parties (e.g., our third party service providers, advertisers, advertising networks and platforms, agencies, other marketers, magazine publishers, retailers, participatory databases, and non-profit organizations) that wish to market products or services to you. If you wish to opt out from such sharing please visit the How to Contact Us/Opting Out of Marketing Communications section below for further instructions.</li>
</ul>
<p>Please note that the Services covered by this Privacy Policy may offer content (e.g., contests, sweepstakes, promotions, games, applications, or social network integrations) that is sponsored by or co-branded with identified third parties. By virtue of these relationships, the third parties may obtain information from their visitors. We have no control over these third parties' use of this information, which is subject to their own privacy policies.</p>
<h3>Cookies and Other User and Ad-Targeting Technologies.</h3>
<p>We use cookies and other technologies both to provide our Services to you and to advertise to you. We also may work with Partners to help them advertise to you when you visit other websites or mobile applications, and to help them perform user analytics. These technologies may also be used to analyze how our Users interact with advertising on our Services and elsewhere, and more generally, to learn more about our Users and what services or offers you might prefer to receive. We describe some of these technologies below.</p>
<ul class="list"><li>
	Cookies. To enhance your online experience, we and our Partners use “cookies”, “web beacons” or other tracking technologies. Cookies are text files placed in your computer's browser to store your preferences. We use cookies or other tracking technologies to understand Service and Internet usage and to improve or customize the products, content, offerings, services or advertisements on our Services. For example, we may use cookies to personalize your experience at our Services (e.g., to recognize you by name when you return to a Service), save your password in password-protected areas, and enable you to use shopping carts on our Services. We also may use cookies or other tracking technologies to help us offer you products, content, offerings or services that may be of interest to you and to deliver relevant advertising when you visit this Service, a Turner Affiliate’s Service, or when you visit other websites or applications. We or a third party platform with whom we work may place or recognize a unique cookie on your browser to enable you to receive customized content, offers, services or advertisements on our Services or other sites. These cookies contain no information intended to identify you personally. The cookies may be associated with de-identified demographic or other data linked to or derived from data you voluntarily have submitted to us (e.g., your email address) that we may share with a service provider solely in hashed, non-human readable form.</li></ul>
<p>
We, our third party service providers, advertisers, advertising networks and platforms, agencies, or our Partners also may use cookies or other tracking technologies to manage and measure the performance of advertisements displayed on or delivered by or through the Turner Network and/or other networks or Services. This also helps us, our service providers and Partners provide more relevant advertising.</p>
<ul class="list">
    <li>
	<b>Syncing Cookies and Identifiers.</b> We may work with our Partners (for instance, third party ad platforms) to synchronize unique, anonymous identifiers (such as those associated with cookies) in order to match our Partners’ uniquely coded user identifiers to our own. We may do this, for instance, to enhance data points about a particular unique browser or device, and thus enable us or others to send ads that are more relevant, match Users to their likely product interests, or better synchronize, cap, or optimize advertising.</li>
	<li>
	<b>Disabling Cookies.</b> Most web browsers are set up to accept cookies. You may be able to set your browser to warn you before accepting certain cookies or to refuse certain cookies. However, if you disable the use of cookies in your web browser, some features of the Services may be difficult to use or inoperable.</li>
<li>
	<b>	Web Beacons.</b> We and our Partners may also use “web beacons” or clear GIFs, or similar technologies, which are small pieces of code placed on a Service or in an email, to monitor the behavior and collect data about the visitors viewing a Service or email. For example, web beacons may be used to count the users who visit a web page or to deliver a cookie to the browser of a visitor viewing that Service. Web beacons may also be used to provide information on the effectiveness of our email campaigns (e.g., open rates, clicks, forwards, etc.).</li>
	<li>
	<b>Mobile Device Identifiers and SDKs.</b> We also sometimes use, or partner with publishers or publisher-facing or app developer platforms that use, mobile Software Development Kits (“SDKs”), to collect information, such as mobile identifiers (e.g., IDFAs and Android Advertising IDs), and information connected to how mobile devices interact with our Sites and those using our Sites. Such SDKs provide computer code that app developers can include in their apps to enable ads to be shown, data to be collected, and related services or analytics to be performed. We may use this technology to deliver or help our partners and other third parties to deliver certain advertising through mobile applications and browsers based on information associated with your mobile device. If you’d like to opt-out from having ads tailored to you in this way on your mobile device, please follow the instructions in the “Your Ad Choices” section below.</li></ul>
<p>By visiting the Service, whether as a registered user or otherwise, you acknowledge, and agree that you are giving us your consent to track your activities and your use of the Service through the technologies described above, as well as similar technologies developed in the future, and that we may use such tracking technologies in the emails we send to you.</p>
<p>Our unaffiliated Partners and third parties may themselves set and access their own tracking technologies when you visit our Services and they may have access to information about you and your online activities over time and across different websites or applications when you use the Service. Their use of such tracking technologies is not in our control and is subject to their own privacy policies.</p>
<h3>Your Ad Choices</h3>
<p>Advertising enables us to provide the rich content for which Services on the Turner Network are known, and to provide much of this content free of cost to our Users. Thus, we strive to provide you with ads that are relevant to you and for products you may want.</p>
<p>The tools provided on the Ad Choices page, as well as the DAA opt-out page and the NAI opt-out page are provided by third parties, not spotrum. spotrum does not control or operate these tools or the choices that advertisers and others provide through these tools.</p>
<p>When using a mobile application, you may receive tailored in-application advertisements. Depending on your device, you may be able to reset your mobile device’s advertising identifier at any time by accessing the privacy settings on your mobile device. In addition, each operating system, iOS for Apple phones, Android for Android devices and Windows for Microsoft devices, provides its own instructions on how to prevent the delivery of tailored in-application advertisements. You may review the support materials and/or the privacy settings for the respective operating systems in order to opt-out of tailored in-application advertisements. For any other devices and/or operating systems, please visit the privacy settings for the applicable device or contact (or review the applicable privacy web page of) the applicable platform operator.</p></li>

<h3>Other Important Information About Your Privacy</h3>

<h4>How We Respond to Do Not Track Signals</h4>
<p>At this time Services on the Turner Network do not recognize automated browser signals regarding tracking mechanisms, which may include “do not track” instructions.</p>
<h4>Linked Services</h4>
<p>Some of the Services contain links to or integrations with other services such as Facebook, Twitter, LinkedIn, and other media services and platforms whose information practices may be different than ours. Visitors should consult these other services' privacy notices as we have no control over information that is submitted to, or collected by, these third parties.</p>
<h4>Data Security</h4>
<p>We have put in place electronic and managerial procedures designed to help prevent unauthorized access, to maintain data security, and to use correctly the Information we collect online. These safeguards vary based on the sensitivity of the Information that we collect and store.</p>
<p>Although we take appropriate measures to safeguard against unauthorized disclosures of Information, we cannot assure you that Information will never be disclosed, altered or destroyed in a manner that is inconsistent with this Privacy Policy.</p>
<h4>How You Can Access or Correct Information</h4>
<p>Access to certain personal Information that is collected from our Services and that we maintain may be available to you. For example, if you created a password-protected account within our Service, you can access that account to review account Information.</p>
<p>You may also send an e-mail or letter to the following e-mail or street address requesting access to or correction of your personal Information. Please include your registration information for such service such as first name, last name and e-mail address in the request. We may ask you to provide additional information for identity verification purposes, or to verify that you are in possession of an applicable email account.</p>

<h4>How to Contact Us/Opting Out of Marketing Communications</h4>
<p>If you have any questions or concerns about the online Privacy Policy for this Service or its implementation, or if you would like to opt out from our sharing of your personal Information with unaffiliated third parties for the third parties' direct marketing purposes you may contact our Privacy Policy Coordinator at the above address and request that we opt you out of such sharing. We may ask you to provide additional information for identity verification purposes, or to verify that you are in possession of an applicable email account.</p>
<p>If you have signed up to receive our e-mails and prefer not to receive marketing information from this Service, follow the “unsubscribe” instructions provided on any marketing e-mail you receive from this Service. If you have signed up to receive text messages from us and no longer wish to receive such messages, you may follow the instructions to stop the delivery of such messages, which may include by replying “STOP” to the received text message.</p>
<h4>Updates & Effective Date</h4>
<p>From time to time, we may update this Privacy Policy. We will notify you about material changes by either sending an email message to the email address you most recently provided to us or by prominently posting a notice on our Service. We encourage you to periodically check back and review this policy so that you always will know what information we collect, how we use it, and with whom we share it.</p>
<h3>KEY POINTS</h3>
<ul class="list">
<li>We use Information to allow you to use the Services. </li>
<li>If you give us permission, we will send you news and promotional material about our Services. You can change your mind about this at any time and ask us not to process your Information for direct marketing purposes, including profiling related to direct marketing. This policy explains when we process Information, for our legitimate interests. You can ask us to stop processing this Information. </li>
<li>We use cookies and other tracking technologies to personalize content and advertising and to make our content function better.</li>
<li>We provide interactive features that engage with social media sites, such as Facebook and Twitter. If you use these features, these sites will send us Information about you. </li>
<li>We disclose Information when you agree that we can share Information with business partners or other Turner Affiliates. We also share Information with third parties we engage to process Information on our behalf or when such sharing is required by law, or in certain other situations. </li>

<h4>HOW WE USE YOUR INFORMATION</h4>
<p>We use your Information for the following purposes:</p>
<p>1.	To fulfil a contract, or take steps prior to entry into a contract:</p>
<ul class="list">
    <li>to process your registration for the Services, or entry to a competition;</li>
    <li>to provide the Services, to communicate with you about them or your account with us;</li>
    <li>to send you information about changes to our terms or policies; and</li>
    <li>to verify your identity.</li>
</ul>
<p>2.	Where this is necessary for purposes which are in our, or third parties', legitimate interests:</p>
<p>These interests are:</p>
<ul class="list">
<li>to fulfil your requests and inquiries regarding the Services and to notify you if you have won a contest;</li>
<li>to provide technical support for the Services;</li>
<li>to analyse how you use the Services, including which videos you like to watch, and to help us develop new services; and</li>
<li>to enforce our policies and terms of use, to check for potential illegal activity (such as copyright infringement or fraud) and to protect the safety of other users of the Services.</li>
</ul>
<p>3.	Where you give us consent:</p>
<ul class="list">
<li>to send you newsletters you have requested and to use technologies to check if these have been received and opened; and</li>	
<li>to send you content, offers, rewards, and promotional materials, by email or on other sites, apps or social media, which may be personalized based on your visits to and use of the Services and to check to see if these ads have been received and if they were of interest to you.</li>	
</ul>
<p>4.	For purposes which are required by law;</p>
<ul class="list">
    <li>in response to requests by government or law enforcement authorities conducting an investigation.</li>
</ul>
<h4>INFORMATION WE RECEIVE</h4>
<p>1.	From You. We receive Information that you provide to us directly, such as registration Information, and Information that is collected automatically, such as Information collected from your browser or device.</p>
<p>2.	From Third Parties. We also receive some Information about you from third parties.</p>
<p><b>When you engage with our content through social media, or other non-Turner, sites or applications, those sites will share Information with us:</b></p>
<ul class="list"><li>
a.	about the content you have viewed or interacted with, your game performance and high scores, and about adverts within the content which you have been shown or clicked on; and</li>
<li>
b.	in your profile – for example, your name, e-mail address, gender, birthday, location, user ID, photos and videos, your list of friends or connections, people you follow and/or who follow you, or your posts or "likes".
</li></ul>
<p>The privacy policies for these sites and applications will contain more detail about this and how to change your privacy settings on those sites and applications.</p>
<h4>INFORMATION SHARING AND DISCLOSURE</h4>
<p>We share your Information:</p>
<ul class="list">
   <li> 1.	With service providers we have engaged to perform business-related functions on our behalf. For example, to: (a) conduct research and analytics; (b) create content; (c) provide customer support services; (d) conduct or support marketing; (e) fulfil orders; (f) handle payments; (g) host Services; (h) administer contests; and (i) maintain databases;
</li>
<li>2.	In response to legal process, for example, in response to a court order or a subpoena, a law enforcement or government agency's request;
</li>
<li>3.	With third parties if this will help us to enforce our policies and terms of use, to check for potential illegal activity (such as copyright infringement or fraud) or to protect the safety of other users of the Services;
</li>
<li>4.	If we, or one of our business units, undergoes a business transition, like a merger, acquisition by another company, or sale of all or part of our assets;
</li>
<li>5.	With your consent, we share Information with other organisations so they can market their products and services to you.
</li> </ul>
<h4>IV.	COOKIES AND SIMILAR TRACKING TECHNOLOGIES.</h4>
<p>We use cookies and other similar technologies to help provide Services, to advertise to you and to analyse how you use our Services and whether advertisements are being viewed. We also allow third parties to use tracking technologies for similar purposes.</p>
<p>We use tracking technologies for the following purposes:</p>
<p><b>Strictly necessary purposes</b></p>
<p>To let you login, to ensure site security and to provide shopping cart functionality. Without this type of technology, our Services won't work properly or won't be able to provide certain features and functionalities.</p>
<p><b>Performance purposes</b></p>
<p>To analyse how visitors use a website, for instance which pages visitors visit most often, in order to provide a better user experience. We also use this technology to check if you have opened our emails, so we can see if they are being delivered correctly and are of interest.</p>
<p><b>Personalisation purposes</b></p>
<p>To remember choices you have made — such as language or region.</p>
<h4>Advertising cookies</h4>
<p>To limit the number of times you see an advertisement, or to customize advertising across Services and make it more relevant to you and to allow us to measure the effectiveness of advertising campaigns and track whether ads have been properly displayed.</p>
<p><b>Social media cookies</b></p>
<p>Cookies are used by social media services to enable you to share our content with your friends and networks. These cookies may track your browser across other sites and build a profile of your interests, which may impact the content and messages you see on other websites that you visit.</p>
<p><b>How to manage & remove cookies</b></p>
<p>If you are using our Services via a browser you can restrict, block or remove cookies through your web browser settings. The Help menu on the menu bar of most browsers also tells you how to prevent your browser from accepting new cookies, how to delete old cookies, how to have the browser notify you when you receive a new cookie and how to disable cookies altogether.</p>
<p>We also use Flash cookies to provide some content such as video clips or animation. You can manage Flash cookies via Adobe's website.</p>
<p>If you are using our Services via an application, the operating system for your device provides instructions on how to prevent tailored advertising and how to reset your device's advertising identifier.</p>
If you would like to contact us about cookies please email us at <a href="mailto:contactus@spotrum.com" target="_top">contactus@spotrum.com</a>
<h4>V.	OTHER IMPORTANT INFORMATION ABOUT YOUR PRIVACY</h4>
<p><b>	Data Security</b></p>
<p>We have put in place physical, electronic, and managerial procedures designed to help prevent unauthorized access, to maintain data security, and to use correctly the Information we collect online. These safeguards vary based on the sensitivity of the Information that we collect and store.</p>
<p>Although we take appropriate measures to safeguard against unauthorized disclosures of Information, we cannot assure you that Information will never be disclosed, altered or destroyed in a manner that is inconsistent with this Privacy policy.</p>
<p><b>Retention of Your Information</b></p>	
<p>We keep details of users who have registered for a Service with us for as long as you use the Service and for up to 18 months afterwards.
If you make payments in connection with the Services, we keep payment data for 13 months after the date of payment.
If you contact us with support queries, we keep the data for between 3 and 18 months, depending on the nature of the query and whether you have registered with us for a Service.</p>
<p>Our cookies and other tracking technologies expire 13 months from the date after you give or renew consent for us to use these technologies.</p>
<p>Where we process Information for marketing purposes, we process the Information while you remain interested in the marketing (for example, where you open and read our offers) and for three years after that point. If you ask us to stop sending marketing we will keep your Information for a short period after this (to allow us to implement your requests). We also keep a record of the fact that you have asked us not to send you direct marketing or to process your Information indefinitely so that we can respect your request in the future.</p>
<p>Where we process Information for Service security purposes, we retain it for 6 months.</p>
<h4>VI.	UPDATES & EFFECTIVE DATE</h4>
<p>From time to time, we may update this policy. We will notify you about material changes by either sending an email message to the email address you most recently provided to us or by prominently posting a notice on our Service. We encourage you to periodically check back and review this policy so that you know what Information we collect, how we use it, and with whom we share it.</p>
</div>
</div>
@endsection