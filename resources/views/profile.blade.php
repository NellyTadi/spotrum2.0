@extends('layouts.app')

@section('body')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
 <style type="text/css">

.select2-selection__choice__remove {
    display: none !important;
}
 label{
    font-weight: 600;
    font-size: 14px;
 }
 .shift{
    margin-left: 10px;
 }
 .social-list li, .shift .fa-edit{
    display: inline;
    padding: 10px;
    margin: 5px;
    font-size: 16px;
 }
 .social-list .facebook:hover,.shift .fa-edit:hover{
    color: #3C5A99;
 }
 .social-list .instagram:hover{
    color: #E1306C;
 }
  .social-list .twitter:hover{
    color: #1DA1F2;
 }
 .hide{
    display: none;
 }
 form label{
    color: #000;
 }
 input[type=text],input[type=url],input[type=number],input[type=date]{
    border: none;
    font-size: 12px;
    width: 100%;
    border-bottom: 1px solid #B3B3B3;

 }
  input[type=text]:focus,input[type=url]:focus,input[type=number]:focus,input[type=date]:focus{

    border:  1px solid #B3B3B3;
  }
input[type=file]{
    width: 100%;
    height: 100%;
    opacity: 0;
}
.select-file{
    border-bottom: 1px solid #b3b3b3;
    position: absolute;
    font-size: 12px;
    border-radius: 3px;
}

  ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  color: #B3B3B3;
  font-size: 12px;
}
::-moz-placeholder { /* Firefox 19+ */
  color: #B3B3B3;
  font-size: 12px;
}
:-ms-input-placeholder { /* IE 10+ */
  color: #B3B3B3;
  font-size: 12px;
}
:-moz-placeholder { /* Firefox 18- */
  color: #B3B3B3;
  font-size: 12px;
}
 </style>

 
<div class="container" >
    @foreach ($errors->all() as $message) 
       {{$message}}
       @endforeach
    <div class="row">
              <div class="col-md-3" style="padding: 20px;background: #fff;margin:20px 0px;">
      <ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link" href="{{url('/')}}">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{url('posts/created')}}">My posts</a>
  </li>
  @if(Auth::user()->isAdmin == 1)
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/posts/created')}}">View all posts</a>
  </li>
    <li class="nav-item">
    <a class="nav-link " href="{{url('create/ads')}}">Create new ads <i class="fa fa-plus"></i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/users')}}">View users</a>
  </li>
  @endif
  <li class="nav-item">
    <a class="nav-link" href="{{url('/profile')}}">My profile</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{route('change.password')}}">Security and privacy</a>
  </li>
</ul>
</div>

       
        <div class="col-md-9" style="padding: 20px;background: #fff;margin:20px 0px;">
              <h5 class="heading-design-h5">Profile</h5>
              <form action="{{route('profile.update')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="row">
                <div class="col-md-4">
                    <div style="max-height: 300px; overflow: hidden;">
                @if(Auth::user()->avatar == '')
                <img src="{{asset('images/icon/human.png')}}" class="img-fluid" id="avatar" alt="profile picture" >
                @else
                <img src="{{asset('images/profile/'.Auth::user()->avatar)}}" class="img-fluid" id="avatar" alt="profile picture">
                @endif
                </div>
                <div class="col-md-12 mx-auto" style="position: relative;">
                     <span class="select-file" style="text-align: center;">Change profile picture <i class="fa fa-picture-o"></i></span>
                    <input type="file" name="avatar" accept="image/*" placeholder="change profile picture" onchange="openFile(event)"/>
                </div>
                
            </div><br>

            <div class="col-md-8">
               <div class="row">
                <div class="col-md-6">
            <label>Username</label>
                <div class="shift shift1"> {{Auth::user()->name}} <i class="fa fa-edit" id="edit-name" title="Edit username"></i>
                    <div class="hide" id="name-text">
                        <input type="text" class="form-control" value="{{Auth::user()->name}}" name="name" placeholder="Enter new username" >
                    </div>
                </div>
              </div>
              <div class="col-md-6">
            <label>Email</label>
                <div class="shift"> {{Auth::user()->email}}</div><br>
              </div>
            </div>
                <div class="row">
                <div class="col-md-6">
                <label>First name</label>
                <input type="text" name="firstname" class="form-control" value="{{Auth::user()->firstname}}"><br>
              </div>
              <div class="col-md-6">
                <label>Last name</label>
                <input type="text" name="lastname" class="form-control" value="{{Auth::user()->lastname}}"><br>
              </div>
            </div>
                <label>Phone number</label>
                <input type="number" name="phonenumber" class="form-control" value="{{Auth::user()->phonenumber}}" pattern="[0-9]{3}-[0-9]{4}-[0-9]{3}" ><br>
            <div class="row">
                <div class="col-md-6">
                <label>Date of birth</label>
                <input type="Date" name="dateofbirth" class="form-control" value="{{Auth::user()->dateofbirth}}"><br>
              </div>
              <div class="col-md-6">
                <label>Nationality</label>
                <select class="form-control" name="nationality">
                  <option value="{{Auth::user()->nationality}}" selected>{{Auth::user()->nationality}}</option>
                  @foreach($country as $cunt)
                  <option value="{{$cunt->country}}" >{{$cunt->country}}</option>
                  @endforeach
                  <option value=""></option>
                </select>
               
              </div>
            </div>
            
            <label>Interests & Categories</label>
                <div class="shift">
                    <select id="country" name="post_tag_interests[]" multiple style="width: 100%;"> 
                        @if(Auth::user()->post_tag_interests != '')
                        <?php
                        $post_tag=json_decode(Auth::user()->post_tag_interests,true);
                        if ($post_tag['post_tag_interests']!='') {
                          
                        $keys=array();
                        foreach ($post_tag['post_tag_interests'] as $key) {
                           echo '<option value="'.$key.'" selected>'.$key.'</option>';
                            $keys[]=$key;
                        }
                        $tags=array();
                        foreach($posttags as $tag){
                            $tags[]=$tag->tag;
                        }
                        
                        $result=array_diff($tags,$keys);
                        foreach ($result as $val) {
                            echo '<option value="'.$val.'">'.$val.'</option>';
                        }

                        }
                        else{
                          foreach($posttags as $tag){
                          echo '<option value="'.$tag->tag.'" >'.$tag->tag.'</option>';
                          }
                        }
                        ?>
                        @else
                        @foreach($posttags as $tag)
                          <option value="{{$tag->tag}}">{{$tag->tag}}</option>
                        @endforeach
                        @endif
                       
                    </select>
                </div>
              <br>
            <label>Linked Accounts</label>
               <div class="shift"> 
                <ul class="social-list">
                    <li class="facebook" id="facebookss" title="facebook" ><i class="fa fa-facebook"></i></li>
                    <li class="instagram" id="instagramss" title="instagram" ><i class="fa fa-instagram"></i></li>
                    <li class="twitter" id="twitterss" title="twitter"><i class="fa fa-twitter"></i></li>
                    
                </ul>
                @if(Auth::user()->social_media != '')
                <?php
                $social_media=json_decode(Auth::user()->social_media,true);
                echo '<div id="facebook-text" class="hide">
                    <input type="url" name="facebook" class="form-control" value="'.$social_media['facebook'].'" placeholder="Enter link to your Facebook account">
                </div><br>
                <div id="instagram-text"  class="hide">
                    <input type="url" name="instagram" class="form-control"value="'.$social_media['instagram'].'" placeholder="Enter link to your Instagram account">
                </div><br>
                <div id="twitter-text" class="hide">
                   <input type="url" name="twitter" class="form-control" value="'.$social_media['twitter'].'"  placeholder="Enter link to your Twitter account">
                </div><br>';
                ?>
                @else
                <div id="facebook-text" class="hide">
                    <input type="url" name="facebook" class="form-control"  placeholder="Enter link to your Facebook account">
                </div><br>
                <div id="instagram-text"  class="hide">
                    <input type="url" name="instagram" class="form-control" placeholder="Enter link to your Instagram account">
                </div><br>
                <div id="twitter-text" class="hide">
                   <input type="url" name="twitter" class="form-control"  placeholder="Enter link to your Twitter account">
                </div><br>
                <!-- Button trigger modal -->

                @endif
            
          </div>
            <button  class="btn btn-theme-round" style="float: right;">Update</button>
        </div>
        </div>
        </form>
        </div>
    </div>
</div>

<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){
    $("#facebookss").click(function() {
        $("#facebook-text").toggle(300);
    });
    $("#instagramss").click(function() {
        $("#instagram-text").toggle(300);
    });
    $("#twitterss").click(function() {
        $("#twitter-text").toggle(300);
    });
    $("#edit-name").click(function() {
        $("#name-text").toggle(300);
    });
});
$('select').select2({
  allowClear: true
});
      var openFile = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
      var dataURL = reader.result;
      var output = document.getElementById('avatar');
      output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
     $("#update-btn").show(300);
  }
</script>
@endsection

