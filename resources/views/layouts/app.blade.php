<!doctype html>
<html lang="en">
<head>
    <!-- Basic Page Needs =====================================-->
    <meta charset="utf-8">
    
    <!-- Mobile Specific Metas ================================-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Spotrum - Fellowship of Sport</title>
    <meta name="description" content="Spotrum - Bringing spot on sport news and the fellowship of the game to you" />
    <meta property="og:title" content="Spotrum" />
    <meta property="og:description" content="Spotrum - Bringing spot on sport news and the fellowship of the game to you" />
    <meta property="og:url" content="https://www.spotrum.com/" />
    <meta property="og:image" content="{{asset('images/logo/Spotrum-Logo.jpg')}}" />
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon" />
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/png">
    <!-- Site Title- -->
    

    <!-- CSS
   ==================================================== -->
   
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  

    <!-- navigation -->
    <link rel="stylesheet" href="{{asset('css/navigation.css')}}">

   
    <!-- Style -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- color -->
    <link rel="stylesheet" href="{{asset('css/colors/color-1.css')}}">

    <!-- Responsive -->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-104672365-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-104672365-4');
</script>
</head>
<nav class="navbar navbar-expand-lg navbar-light " style="background-color: #0d1943;">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#"><img src="{{asset('images/logo/logo - Copy.png')}}" alt=""></a>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    
    <ul class="navbar-nav mr-auto">
      
       <li class="nav-item left-nav-o active"><a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a></li>
                             <?php
                            $i=0;
                            foreach($posttags as $tag) { 
                               echo '<li class="nav-item left-nav-o"><a class="nav-link"  href="/search/'.$tag->tag.'">'.$tag->tag.'</a></li>';
                               $i++;
                               if ($i>=6) {
                                  break;
                               }
                           }
                           ?>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        All Sport
        </a>

          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="">
            <div class="row no-gutters">
                <div class="col-6 right-border">
             <?php
                $i=0;
                $mid=count($posttags)/2;
                 foreach($posttags as $tag) { 
                   $i++;
                    if ($i <= $mid) {

                         echo' <a class="dropdown-item allsport-list" href="/search/'.$tag->tag.'">'.$tag->tag.'</a>  ';
                    }
                }
               ?>                 
            
         </div>
          <div class="col-6">
             <?php
                $i=0;
                $mid=count($posttags)/2;
                 foreach($posttags as $tag) { 
                   $i++;

                    if ($i > $mid) {
                         echo' <a class="dropdown-item allsport-list" href="/search/'.$tag->tag.'">'.$tag->tag.'</a>  ';
                    }
                }
               ?>                 
            
         </div>
     </div>
          </div>
        
    </li>
   </ul>
    <ul class="navbar-nav ml-auto">
         <li class="nav-item"  id="search-form"> 
                            <form action="{{url('/search/tag')}}" method="GET">
                                <span class="nav-search-close-button" tabindex="0">✕</span>
                                    <div class="nav-search-inner">
                                        <input type="search" name="tag" placeholder="Type and hit ENTER">
                                    </div>
                            </form>
                        </li>
                       
                        @if(Auth::check())
                        @if(count($notifications)>0)
                         <li class="left-nav nav-item dropdown" >
                          <a class="dropdown-toggle dropdown-toggle-right" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell-o" style="color: red;padding-right: 10px;margin-right: 0;"></i>
                            </a>
                            <div class="dropdown-menu p-4 text-muted"  aria-labelledby="navbarDropdown3" style="width: 300px;">
                                        @foreach($notifications as $notif)
                                         <p><a href="{{url('/post/'.$notif->post_id.'/'.$notif->posttitle)}}">{{str_limit($notif->notification,50)}}
                                         <?php echo date('M d, Y',strtotime($notif->created_at));?> 
                                           </a></p><hr>
                                        @endforeach           
                            </div>
                                  

                            </li>
                            @else
                            <li class="left-nav dropdown nav-item">
                              <a class="dropdown-toggle dropdown-toggle-right" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell-o left-nav-ico" ></i></a>
                                    <div class="dropdown-menu"  aria-labelledby="navbarDropdown4" style="width: 300px;">
                                       <p class="dropdown-item notif-list">You have no notifications.</p>
                                          
                                    </div>  

                            </li>
                            @endif
                        <li class="left-nav dropdown nav-item " >
                            <a class="dropdown-toggle dropdown-toggle-right" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             <i class="fa fa-user-circle-o left-nav-ico"></i>
                            </a>
                                
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2" style="right: 250px;">
                                  <a class="dropdown-item allsport-list" href="{{url('/home')}}">New post</a>
                                  <a class="dropdown-item allsport-list" href="{{url('posts/created')}}">My Posts</a>
                                  @if(Auth::user()->isAdmin == 1)
                                  <a class="dropdown-item allsport-list" href="{{url('all/posts/created')}}">All Posts</a>
                                  <a class="dropdown-item allsport-list" href="{{url('create/ads')}}"> Create Ads </a>
                                  @endif
                                  <a class="dropdown-item allsport-list" href="{{url('/profile')}}">Profile</a>
                                  <a class="dropdown-item allsport-list" href="{{route('change.password')}}">Privacy and Security</a>
                                  <a class="dropdown-item allsport-list"  href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                   </div>
                                
                              </li>
                         
                        
                        
                        @else
                        <li class="left-nav nav-item" style="margin-top: 5px;">
                            <a class="nav-link" href="{{ url('/login') }}" data-toggle="modal" data-target="#bd-example-modal">Login</a>
                        </li>
                        <li class="left-nav nav-item " style="margin-top: 5px;">
                            <a class="nav-link" href="{{ url('/register') }}">Register</a>
                        </li>
                         @endif
                        
                      <li class="nav-item left-nav" title="search" style="padding-right: 40px; margin-top: -15px;"> 
                            <a class="nav-link btn-search"><i class="fa fa-search left-nav-ico" ></i></a>
                        </li>
                    </div>
                </ul>  
            </div>
</nav>

     @if (session('status'))
              <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                 {{ session('status') }}
              </div>
    @endif
<!-- header nav end-->

@yield('body')

 <a data-toggle="modal" data-target="#post-modal" href="{{url('/home')}}"><button class=" new-blog-btn"><img src="{{asset('images/icon/blog.png')}}"></button></a>
<!-- footer area -->
<div class="footer-area">
    <!-- footer start -->
    <footer class="ts-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-menu text-center">
                        <ul>
                           
                            <li>
                                <a href="{{url('/privacy-policy')}}">Privacy</a>
                            </li>
                            
                            <li>
                                <a href="{{url('/terms-and-conditions')}}">Terms and conditions</a>
                            </li>
                        </ul>
                    </div>
                    <div class="copyright-text text-center">
                        <p>&copy; <?php echo date('Y'); ?>, SportForum is a product of Koete Synergy Limited.</p>
                    </div>
                    
                    <div class="copyright-text text-center" ><p>Managed by <a href="https://www.cinoteck.com.ng" style="color:#63B6F3;" target="_blank">Cinoteck</a></p></div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->
</div>
<!-- footer area end-->

<div class="modal fade login-modal-main" id="bd-example-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="login-modal">
                    <div class="row">
                        <div class="col-lg-6 pad-right-0">
                            <div class="login-modal-left">
                                <img src="{{asset('images/login_register_bg.png')}}">
                            </div>
                        </div>
                        <div class="col-lg-6 pad-left-0">
                            <button type="button" class="close close-top-right" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                           
                                <div class="login-modal-right">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="login" role="tabpanel">
                                            <h5 class="heading-design-h5">Login to your account</h5>
                                             <form method="POST" action="{{ route('login') }}">
                                                  {{csrf_field()}}
                                            <fieldset class="form-group">
                                                <label for="formGroupEmailInput">Enter Email</label>
                                                <input type="email" class="form-control" id="formGroupEmailInput"
                                                       placeholder="johndoe@example.com" name="email">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label for="formGroupExampleInput2">Enter Password</label>
                                                <input type="password" class="form-control" id="formGroupPasswordInput2"
                                                       placeholder="********" name="password">
                                            </fieldset>
                                              <p><label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                                <input type="checkbox" name="remember" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description" >Remember me </span>
                                            </label>
                                            </p>
                                            <fieldset class="form-group">
                                                <button type="submit" class="btn btn-lg btn-theme-round btn-block">Log in
                                                    to your account
                                                </button>
                                            </fieldset>
                                           <div style="text-align: center;">
                                <a  href="{{ route('password.request') }}" >
                                    Forgot Your Password?
                                </a>
                                </div>
                                        </form>

                                            <div class="login-with-sites text-center">
                                                <p>or Login with your social profile:</p>
                                               <a href="{{url('login/facebook')}}">
                        <button class="btn-facebook login-icons btn-lg">
                            <i class="fa fa-facebook"></i> Facebook
                        </button>
                    </a>
                    <a href="{{url('login/google')}}">
                        <button class="btn-google login-icons btn-lg">
                            <i class="fa fa-google"></i> Google
                        </button>
                    </a>
                                            </div>
                                           
                                        </div>
                                        <div class="tab-pane" id="register" role="tabpanel">
                                            <h5 class="heading-design-h5">Register Now!</h5>
                                            <form method="POST" action="{{ route('register') }}">
                                                {{csrf_field()}}
                                            <fieldset class="form-group">
                                                <label for="formGroupExampleInput">Enter Email</label>
                                                <input type="text" class="form-control" id="formGroupExampleInput" name="email" 
                                                       placeholder="johndoe@example.com" required>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label for="formGroupExampleInput2">Enter Password</label>
                                                <input type="password" class="form-control" id="formGroupExampleInput2" name="password" 
                                                       placeholder="********" required>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label for="formGroupExampleInput3">Enter Confirm Password </label>
                                                <input type="password" class="form-control" name="password_confirmation" id="password-confirm"
                                                       placeholder="********" required>
                                            </fieldset>
                                             <p>
                                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                                    <input type="checkbox" class="custom-control-input" required>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">I Agree with<a href="{{url('/terms-and-conditions')}}"> Term and Conditions </a> </span>
                                                </label>
                                            </p>
                                            <fieldset class="form-group">
                                                <button type="submit" class="btn btn-lg btn-theme-round btn-block">
                                                    Create Your Account
                                                </button>
                                            </fieldset>
                                           
                                        </form>
                                        <div class="login-with-sites text-center">
                                                <p>or Sign up with your social profile:</p>
                                               <a href="{{url('login/facebook')}}">
                        <button class="btn-facebook login-icons btn-lg">
                            <i class="fa fa-facebook"></i> Facebook
                        </button>
                    </a>
                    <a href="{{url('login/google')}}">
                        <button class="btn-google login-icons btn-lg">
                            <i class="fa fa-google"></i> Google
                        </button>
                    </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="text-center login-footer-tab">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#login" role="tab"><i
                                                        class="icofont icofont-lock"></i> LOGIN</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#register" role="tab"><i
                                                        class="icofont icofont-pencil-alt-5"></i> REGISTER</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- New Blog Post Modal-->

<div class="modal fade login-modal-main" id="post-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="login-modal">
                    <div class="row">
                      
                        <div class="col-lg-12 pad-left-0">
                            <button type="button" class="close close-top-right" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                           
                                <div class="login-modal-right">
                                     <h5 class="heading-design-h5">Create blog post</h5>
                                     <form action="{{url('post/submit')}}" method="POST" id="post-form" enctype="multipart/form-data"  onsubmit="return checkForm()">
                {{csrf_field()}}
                <div class="blog_img" style="position: relative;text-align: center;">
                  <label>Selet cover image for blog post</label><br>
                            <img src="{{asset('images/banner/temp.jpg')}}" id="preview" class="img-fluid" />
                           
                              <input type="file" name="postcoverimage" accept="image/*" id="image_file" onchange="fileSelectHandler()" title="select cover picture for blog post" class="form-control">
                              <div class="error"></div>
                              <input type="hidden" id="x1" name="x1" />
                              <input type="hidden" id="y1" name="y1" />
                              <input type="hidden" id="x2" name="x2" />
                              <input type="hidden" id="y2" name="y2" />
                              <input type="hidden" id="w" name="w" />
                              <input type="hidden" id="h" name="h" />
                              <input type="hidden" id="filesize" name="filesize" />
                             <input type="hidden" id="filetype" name="filetype" />
                              <input type="hidden" id="filedim" name="filedim" />
                           
                  </div>

                <label>Post Title</label>
                <input type="text" name="posttitle" class="form-control" required placeholder="Post title"><br>
              
<label>Select tag relating to post</label>
    <select required name="posttags" class="form-control">
        <option disabled></option>
        @foreach($posttags as $tag)
        <option value="{{$tag->tag}}"> {{$tag->tag}} </option>
        @endforeach
    </select>
<br>

<label>Add Youtube Video Link</label>
<input type="url" name="youtube_video" class="form-control" placeholder="* optional- enter URL to youtube video">

<input type="hidden" name="postbody" required>
<label>Post</label>
                <div id="editor">
                    
                </div><br>
                <button class="btn btn-lg btn-theme-round btn-block">Publish</button>
            </form>
       
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>


<!--End of New blog Post Modal-->
<!-- javaScript Files
=============================================================================-->

<!-- initialize jQuery Library -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- navigation JS -->
<script src="{{asset('js/navigation.js')}}"></script>
<!-- Popper JS -->
<script src="{{asset('js/popper.min.js')}}"></script>
<!-- Owl Carousel -->
<script src="{{asset('js/owl-carousel.2.3.0.min.js')}}"></script>
<!-- magnific popup JS -->
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<!-- Bootstrap jQuery -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- Owl Carousel -->
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="{{asset('js/jcrop/js/jquery.Jcrop.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>
