@extends('layouts.app')
@section('body')
<style type="text/css">
    .carousel-img{
        height: 420px;
        width: 1200px;
       
    }
    .carousel-caption{
         background-color: rgba(0, 0, 0, 0.5);
         border-radius: 5px;
    }
    .carousel-caption h5 a{
        color: #fff;
        font-size: 20px;
    }
    .carousel-caption p span{
        font-size: 14px;
        color: #fff;
        padding: 10px;
    }
    .ts-post-thumb{
        max-height: 120px;
    }


    @media(max-width: 992px){
         #side-twitter{
        height: 600px;
        overflow: scroll;
    }
     .carousel-caption h5{
        font-size: 14px;
     }

    }
    @media(min-width: 992px){
        .first-item{
min-height: 350px;
}
    }

</style>


<!-- start blog wrapper -->
<section class="block-wrapper mt-15">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="ts-grid-box ts-col-box">
                    @if(isset($mostviewedpost))
                    <div class="item first-item">
                        <a class="post-cat ts-green-bg" href="{{url('/post/'.$mostviewedpost->id.'/'.$mostviewedpost->posttitle)}}">{{$mostviewedpost->posttags}}</a>
                        <div class="post-content">
                            <h3 class="post-title md">
                                <a href="{{url('/post/'.$mostviewedpost->id.'/'.$mostviewedpost->posttitle)}}">{{str_limit($mostviewedpost->posttitle,100)}}</a>
                            </h3>
                            <span class="post-date-info">
									<i class="fa fa-clock-o"></i>
									 <?php echo date('M d, Y',strtotime($mostviewedpost->created_at));?>
								</span>
                            <p>
                              {{strip_tags(str_limit($mostviewedpost->postbody,280))}}
                            </p>
                            <ul class="post-meta-info">
                                <li>
                                    <a href="{{url('/post/'.$mostviewedpost->id.'/'.$mostviewedpost->posttitle)}}">
                                     <i class="fa fa-user"></i>{{$mostviewedpost->created_by}}
                                    </a>

                                </li>
                           
                             
                                                <span style="float: right; font-size: 14px" class="post-icon">
                                                <a href="{{url('/post/'.$mostviewedpost->id.'/'.$mostviewedpost->posttitle.'/#comment')}}">
                                                    <i class="fa fa-comment-o"></i>
                                                </a>
                                                 <a href="{{url('/post/'.$mostviewedpost->id.'/'.$mostviewedpost->posttitle.'/like')}}">
                                                    <i class="fa fa-thumbs-up"></i>
                                                </a>
                                                </span>
                                    </a>
                                     </ul>
                        </div>
                    </div>
                    @endif
                    <!-- item end-->
                </div>
                <!-- ts-grid-box end-->
            </div>
            <!-- col end-->
            <div class="col-lg-9">
                
@if(isset($adminposts))
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    @foreach($adminposts as $post)
    @if ($loop->first)
    <div class="carousel-item active">
          <img src="{{asset('post/'.$post->postcoverimage)}}" class="img-fluid carousel-img" alt="Slide">
          <div class="carousel-caption d-md-block">
            <h5>
                <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}">
                {{str_limit($post->posttitle,150)}}
                </a>
            </h5>
            <p>
                 <a class="post-cat ts-blue-dark-bg" href="#">{{$post->posttags}}</a><br>
                <span><i class="fa fa-clock-o"></i> <?php echo date('M d, Y',strtotime($post->created_at));?></span>
                <span><i class="fa fa-comment-o"></i> {{count($post->postcomments)}}</span>
            </p>
          </div>
    </div>
    @else
    <div class="carousel-item">
          <img src="{{asset('post/'.$post->postcoverimage)}}" class="img-fluid carousel-img" alt="Slide">
          <div class="carousel-caption d-md-block">
            <h5>
                <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}">
                {{str_limit($post->posttitle,150)}}
                </a>
            </h5>
            <p>
                 <a class="post-cat ts-orange-bg" href="#">{{$post->posttags}}</a><br>
                 <span><i class="fa fa-clock-o"></i> <?php echo date('M d, Y',strtotime($post->created_at));?></span>
                <span><i class="fa fa-comment-o"></i> {{count($post->postcomments)}}</span>
            </p>
          </div>
    </div>
    @endif
    @endforeach
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
@endif
            </div>
            <!-- col end-->
            
        </div>
    </div>
</section>
<!-- end  blog wrapper -->

<!-- start  blog wrapper -->
<section class="block-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="ts-grid-box category-box-item-3">
                    <h2 class="ts-title float-left">Latest Posts</h2>
                    <a href="{{url('/search/tag?tag=')}}" class="view-all-link float-right">View All</a>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <!--latest posts-->
                                @foreach($posts as $post)
                                <div class="col-lg-3">
                                    <div class="item mb-20">
                                         <div class="ts-post-thumb" style="height:100px;">
                                        <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}">
                                            @if($post->youtube_video)
                                            <img class="img-fluid" src="{{asset('images/banner/youtube-logo.jpg')}}">
                                            @elseif($post->postcoverimage)
                                            <img class="img-fluid" src="{{asset('post/thumbnail/'.$post->postcoverimage)}}" alt="">
                                            @else
                                            <img class="img-fluid" src="{{asset('images/banner/thumbnails.jpg')}}" alt="">
                                            @endif
                                        </a>
                                    </div>
                                        <div class="post-content">
                                            <h3 class="post-title" style="height: 65px;">
                                                <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}">{{str_limit($post->posttitle,65)}}</a>
                                            </h3>
                                          <!--  {{str_limit(strip_tags($post->postbody),50)}}-->
                                            <span class="post-date-info">
													<i class="fa fa-clock-o"></i>
												<?php echo date('M d, Y',strtotime($post->created_at));?>
												 <span style="float: right; font-size: 14px" class="post-icon">
                                                <a href="{{url('/post/'.$post->id.'/'.$post->posttitle.'/#comment')}}">
                                                    <i class="fa fa-comment-o"></i>
                                                </a>
                                                 <a href="{{url('/post/'.$post->id.'/'.$post->posttitle.'/like')}}">
                                                    <i class="fa fa-thumbs-up"></i>
                                                </a>
                                                </span>
                                            </span>

                                        </div>
                                       
                                    </div>
                                </div>
                                @endforeach
                                <!-- col end-->
                               
                            </div>
                        </div>
                    </div>
                    <!-- row end-->

                </div>
                <!-- ts-populer-post-box end-->
            @if(Auth::check() && isset($firstauthposts))

            <div class="border-top border-top1 mb-30"></div>
                
                <div class="ts-grid-box clearfix ts-category-title">
                    <h2 class="ts-title float-left">{{$key1}}</h2>
                    <a href="{{url('/search/'.$key1)}}" class="view-all-link float-right">View All</a>
                </div>
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                          
                         @foreach($firstauthposts as $first)
                            <div class="col-lg-3">
                                <div class="item mb-20">
                                    <div class="ts-post-thumb">
                                        <a href="{{url('/post/'.$first->id.'/'.$first->posttitle)}}">
                                             @if($first->youtube_video)
                                            <img class="img-fluid" src="{{asset('images/banner/youtube-logo.jpg')}}" alt="">
                                            @elseif($first->postcoverimage)
                                            <img class="img-fluid" src="{{asset('post/thumbnail/'.$first->postcoverimage)}}" alt="">
                                            @else
                                            <img class="img-fluid" src="{{asset('images/banner/thumbnails.jpg')}}" alt="">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="post-content">
                                        <h3 class="post-title">
                                            <a href="{{url('/post/'.$first->id.'/'.$first->posttitle)}}">{{str_limit($first->posttitle,65)}}</a>
                                        </h3>
                                        <span class="post-date-info">
                                                  <i class="fa fa-clock-o"></i>
                                                <?php echo date('M d, Y',strtotime($first->created_at));?>
                                                <span style="float: right; font-size: 14px" class="post-icon">
                                                <a href="{{url('/post/'.$first->id.'/'.$first->posttitle.'/#comment')}}">
                                                    <i class="fa fa-comment-o"></i>
                                                </a>
                                                 <a href="{{url('/post/'.$first->id.'/'.$first->posttitle.'/like')}}">
                                                    <i class="fa fa-thumbs-up"></i>
                                                </a>
                                                </span>
                                                </span>
                                    </div>
                                </div>
                            </div>
                          @endforeach
                            <!-- col end-->
                        </div>
                    </div>
                  
                </div>
                @if(isset($secondauthposts))
                <div class="border-top border-top1 mb-30"></div>
                
                <div class="ts-grid-box clearfix ts-category-title">
                    <h2 class="ts-title float-left">{{$key2}}</h2>
                    <a href="{{url('/search/'.$key2)}}" class="view-all-link float-right">View All</a>
                </div>
                 <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                          
                         @foreach($secondauthposts as $second)
                            <div class="col-lg-3">
                                <div class="item mb-20">
                                    <div class="ts-post-thumb">
                                        <a href="{{url('/post/'.$second->id.'/'.$second->posttitle)}}">
                                             @if($second->youtube_video)
                                            <img class="img-fluid" src="{{asset('images/banner/youtube-logo.jpg')}}" alt="">
                                            @elseif($second->postcoverimage)
                                            <img class="img-fluid" src="{{asset('post/thumbnail/'.$second->postcoverimage)}}" alt="">
                                            @else
                                            <img class="img-fluid" src="{{asset('images/banner/thumbnails.jpg')}}" alt="">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="post-content">
                                        <h3 class="post-title">
                                            <a href="{{url('/post/'.$second->id.'/'.$second->posttitle)}}">{{str_limit($second->posttitle,65)}}</a>
                                        </h3>
                                        <span class="post-date-info">
                                                  <i class="fa fa-clock-o"></i>
                                                <?php echo date('M d, Y',strtotime($second->created_at));?>
                                                <span style="float: right; font-size: 14px" class="post-icon">
                                                <a href="{{url('/post/'.$second->id.'/'.$second->posttitle.'/#comment')}}">
                                                    <i class="fa fa-comment-o"></i>
                                                </a>
                                                 <a href="{{url('/post/'.$second->id.'/'.$second->posttitle.'/like')}}">
                                                    <i class="fa fa-thumbs-up"></i>
                                                </a>
                                                </span>
                                                </span>
                                    </div>
                                </div>
                            </div>
                          @endforeach
                            <!-- col end-->
                        </div>
                    </div>
                  
                </div>
                @endif
            @else
                <div class="border-top border-top1 mb-30"></div>
                 
                <div class="ts-grid-box clearfix ts-category-title">
                    <h2 class="ts-title float-left">{{$key1}}</h2>
                    <a href="{{url('/search/'.$key1)}}" class="view-all-link float-right">View All</a>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                          
                            @foreach($footballposts as $foot)
                            <div class="col-lg-3">
                                <div class="item mb-20">
                                    <div class="ts-post-thumb">
                                        <a href="{{url('/post/'.$foot->id.'/'.$foot->posttitle)}}">
                                           @if($foot->youtube_video)
                                            <img class="img-fluid" src="{{asset('images/banner/youtube-logo.jpg')}}" alt="">
                                            @elseif($foot->postcoverimage)
                                            <img class="img-fluid" src="{{asset('post/thumbnail/'.$foot->postcoverimage)}}" alt="">
                                            @else
                                            <img class="img-fluid" src="{{asset('images/banner/thumbnails.jpg')}}" alt="">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="post-content">
                                        <h3 class="post-title">
                                            <a href="{{url('/post/'.$foot->id.'/'.$foot->posttitle)}}">{{str_limit($foot->posttitle,65)}}</a>
                                        </h3>
                                        <span class="post-date-info">
											<i class="fa fa-clock-o"></i>
                                                <?php echo date('M d, Y',strtotime($foot->created_at));?>
                                                <span style="float: right; font-size: 14px" class="post-icon">
                                                <a href="{{url('/post/'.$foot->id.'/'.$foot->posttitle.'/#comment')}}">
                                                    <i class="fa fa-comment-o"></i>
                                                </a>
                                                 <a href="{{url('/post/'.$foot->id.'/'.$foot->posttitle.'/like')}}">
                                                    <i class="fa fa-thumbs-up"></i>
                                                </a>
                                                </span>
												</span>
                                    </div>
                                </div>
                            </div>
                          @endforeach
                            <!-- col end-->
                        </div>
                    </div>
                  
                </div>
                <!-- row end-->
                <div class="border-top border-top1 mb-30"></div>

                <div class="ts-grid-box clearfix ts-category-title">
                    <h2 class="ts-title float-left">{{$key2}}</h2>
                    <a href="{{url('/search/'.$key2)}}" class="view-all-link float-right">View All</a>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                          
                            @foreach($basketballposts as $foot)
                            <div class="col-lg-3">
                                <div class="item mb-20">
                                    <div class="ts-post-thumb">
                                        <a href="{{url('/post/'.$foot->id.'/'.$foot->posttitle)}}">
                                            @if($foot->youtube_video)
                                            <img class="img-fluid" src="{{asset('images/banner/youtube-logo.jpg')}}" alt="">
                                            @elseif($foot->postcoverimage)
                                            <img class="img-fluid" src="{{asset('post/thumbnail/'.$foot->postcoverimage)}}" alt="">
                                            @else
                                            <img class="img-fluid" src="{{asset('images/banner/thumbnails.jpg')}}" alt="">
                                            @endif
                                        </a>
                                    </div>
                                    <div class="post-content">
                                        <h3 class="post-title">
                                            <a href="{{url('/post/'.$foot->id.'/'.$foot->posttitle)}}">{{str_limit($foot->posttitle,65)}}</a>
                                        </h3>
                                        <span class="post-date-info">
                                                   <i class="fa fa-clock-o"></i>
                                                <?php echo date('M d, Y',strtotime($foot->created_at));?>
                                                 <span style="float: right; font-size: 14px" class="post-icon">
                                                <a href="{{url('/post/'.$foot->id.'/'.$foot->posttitle.'/#comment')}}">
                                                    <i class="fa fa-comment-o"></i>
                                                </a>
                                                 <a href="{{url('/post/'.$foot->id.'/'.$foot->posttitle.'/like')}}">
                                                    <i class="fa fa-thumbs-up"></i>
                                                </a>
                                                </span>
                                                </span>
                                    </div>
                                </div>
                            </div>
                          @endforeach
                            <!-- col end-->
                        </div>
                    </div>
                  
                </div>

                @endif
            </div>
            <!-- col end-->

            <div class="col-lg-3">
                <div class="right-sidebar">

                    <!-- Login Model here-->
                    <div id="side-twitter">
                    <a class="twitter-timeline" href="https://twitter.com/Spotrum4" data-chrome="nofooter" > Tweets by @Spotrum4 </a>
                </div>


                @if(isset($ads))

                    <div class="widgets widget-banner">
                       <a href="{!!$ads->ad_link!!}" title="{{$ads->ad_owner}}">
                        <img class="img-fluid" src="{{asset('ads/'.$ads->ad_image)}}" alt="Ad">
                        </a>

                    </div>
                    @else
                    <div class="widgets widget-banner">
                        
                        <a data-toggle="modal" data-target="#bd-example-modal" href="#">
                               <img class="img-fluid" src="{{asset('images/banner/Ads-holder.jpg')}}" alt="">
                           </a>

                    </div>
                  @endif
                      <!-- widgets end-->
                    <div class="post-list-item widgets">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation">
                                <a class="active" href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                    <i class="fa fa-clock-o"></i>
                                    Popular tags
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                                    <i class="fa fa-heart"></i>
                                    Popular Posts
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active ts-grid-box post-tab-list" id="home">
                                  <?php
                            $i=0;
                            foreach($posttags as $tag) { 
                               echo '<div class="post-content media">
                                   
                                    <div class="media-body">
                                            <span class="post-tag" >
                                                 <a style="color:#000;" href="/search/'.$tag->tag.'" >'.$tag->tag.'</a>
                                            </span>
                                        
                                    </div>
                                </div>';
                               $i++;
                               if ($i>=6) {
                                  break;
                               }
                           }
                           ?>
                                

                            </div>
                            <!--ts-grid-box end -->

                            <div role="tabpanel" class="tab-pane ts-grid-box post-tab-list" id="profile">
                                @foreach( $popularposts as $post)
                                <div class="post-content media">
                                   
                                    <div class="media-body">
                                            <span class="post-tag">
                                                <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}"> {{$post->posttags}}</a>
                                            </span>
                                        <h4 class="post-title">
                                            <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}">{{str_limit($post->posttitle,65)}} </a>
                                        </h4>
                                    </div>
                                </div>
                               @endforeach
                            </div>
                            <!--ts-grid-box end -->
                        </div>
                        <!-- tab content end-->

                    </div>

              @if(!Auth::check())

                    <div class="widgets widget-banner">
                        
                        <a data-toggle="modal" data-target="#bd-example-modal" href="#">
                               <img class="img-fluid" src="{{asset('images/banner/sidebar-banner1.jpg')}}" alt="">
                        </a>

                    </div>
                    @else
                    <div class="widgets widget-banner">
                        
                        <a href="mailto:contact@spotrum.com?Subject=Request to place ads">
                               <img class="img-fluid" src="{{asset('images/banner/Ads-holder.jpg')}}" alt="">
                           </a>
                    </div>
                    @endif
                    <!-- widgets end-->

                  
        </div>
        <!-- row end-->
    </div>
    <!-- container end-->
</section>
<!-- end  blog wrapper -->


@endsection 
