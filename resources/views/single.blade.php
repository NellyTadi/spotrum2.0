<!doctype html>
<html lang="en">
<head>
    <!-- Basic Page Needs =====================================-->
    <meta charset="utf-8">
    
    <!-- Mobile Specific Metas ================================-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Spotrum - Fellowship of Sport</title>
 	<meta property="og:url" content="https://www.spotrum.com/post/{{$post->id}}/{{$post->posttitle}}" />
 	<meta property="og:type" content="Spotrum" />
    <meta property="og:title" content="{{$post->posttitle}}" />
    <meta property="og:description" content="{{str_limit($post->postbody,50)}}" />
    <meta property="og:image" content="{{asset('images/logo/Spotrum-Logo.jpg')}}" />
   
   
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon" />
    <link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/png">
 
   <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{asset('css/owlcarousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/slick.css')}}">

    <!-- CSS
   ==================================================== -->
   
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  

    <!-- navigation -->
    <link rel="stylesheet" href="{{asset('css/navigation.css')}}">

   
    <!-- Style -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- color -->
    <link rel="stylesheet" href="{{asset('css/colors/color-1.css')}}">

    <!-- Responsive -->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-104672365-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-104672365-4');
</script>
<style type="text/css">
    a:hover{
    cursor: pointer;
  }
    .right-border{
        border-right: 1px #F7F7F7 solid;
        padding:10px;
    }
    .dropdown-menu{
        width: 250px; 
        background-color: #0d1943;
    }
    @media screen and (min-width: 992px) {
    .navbar-nav .active{
        margin-left: 100px;
    }
    .cover-image{
      padding:0px 75px;

    }
}
</style>
</head>
<nav class="navbar navbar-expand-lg navbar-light " style="background-color: #0d1943;">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#"><img src="{{asset('images/logo/logo - Copy.png')}}" alt=""></a>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    
    <ul class="navbar-nav mr-auto">
      
       <li class="nav-item left-nav-o active"><a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a></li>
                             <?php
                            $i=0;
                            foreach($posttags as $tag) { 
                               echo '<li class="nav-item left-nav-o"><a class="nav-link"  href="/search/'.$tag->tag.'">'.$tag->tag.'</a></li>';
                               $i++;
                               if ($i>=6) {
                                  break;
                               }
                           }
                           ?>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        All Sport
        </a>

          <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" style="">
            <div class="row no-gutters">
                <div class="col-6 right-border">
             <?php
                $i=0;
                $mid=count($posttags)/2;
                 foreach($posttags as $tag) { 
                   $i++;
                    if ($i <= $mid) {

                         echo' <a class="dropdown-item allsport-list" href="/search/'.$tag->tag.'">'.$tag->tag.'</a>  ';
                    }
                }
               ?>                 
            
         </div>
          <div class="col-6">
             <?php
                $i=0;
                $mid=count($posttags)/2;
                 foreach($posttags as $tag) { 
                   $i++;

                    if ($i > $mid) {
                         echo' <a class="dropdown-item allsport-list" href="/search/'.$tag->tag.'">'.$tag->tag.'</a>  ';
                    }
                }
               ?>                 
            
         </div>
     </div>
          </div>
        
    </li>
   </ul>
   <ul class="navbar-nav ml-auto">
         <li class="nav-item"  id="search-form"> 
                            <form action="{{url('/search/tag')}}" method="GET">
                                <span class="nav-search-close-button" tabindex="0">✕</span>
                                    <div class="nav-search-inner">
                                        <input type="search" name="tag" placeholder="Type and hit ENTER">
                                    </div>
                            </form>
                        </li>
                       
                        @if(Auth::check())
                        @if(count($notifications)>0)
                         <li class="left-nav nav-item dropdown" >
                          <a class="dropdown-toggle dropdown-toggle-right" href="#" id="navbarDropdown3" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell-o" style="color: red;padding-right: 10px;margin-right: 0;"></i>
                            </a>
                            <div class="dropdown-menu p-4 text-muted"  aria-labelledby="navbarDropdown3" style="width: 300px;">
                                        @foreach($notifications as $notif)
                                         <p><a href="{{url('/post/'.$notif->post_id.'/'.$notif->posttitle)}}">{{str_limit($notif->notification,50)}}
                                         <?php echo date('M d, Y',strtotime($notif->created_at));?> 
                                           </a></p><hr>
                                        @endforeach           
                            </div>
                                  

                            </li>
                            @else
                            <li class="left-nav dropdown nav-item">
                              <a class="dropdown-toggle dropdown-toggle-right" href="#" id="navbarDropdown4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell-o left-nav-ico" ></i></a>
                                    <div class="dropdown-menu"  aria-labelledby="navbarDropdown4" style="width: 300px;">
                                       <p class="dropdown-item notif-list">You have no notifications.</p>
                                          
                                    </div>  

                            </li>
                            @endif
                        <li class="left-nav dropdown nav-item " >
                            <a class="dropdown-toggle dropdown-toggle-right" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             <i class="fa fa-user-circle-o left-nav-ico"></i>
                            </a>
                                
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown2" style="right: 250px;">
                                  <a class="dropdown-item allsport-list" href="{{url('/home')}}">New post</a>
                                  <a class="dropdown-item allsport-list" href="{{url('posts/created')}}">My Posts</a>
                                  @if(Auth::user()->isAdmin == 1)
                                  <a class="dropdown-item allsport-list" href="{{url('all/posts/created')}}">All Posts</a>
                                  <a class="dropdown-item allsport-list" href="{{url('create/ads')}}"> Create Ads </a>
                                  @endif
                                  <a class="dropdown-item allsport-list" href="{{url('/profile')}}">Profile</a>
                                  <a class="dropdown-item allsport-list" href="{{route('change.password')}}">Privacy and Security</a>
                                  <a class="dropdown-item allsport-list"  href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"> Logout</a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                   </div>
                                
                              </li>
                         
                        
                        
                        @else
                        <li class="left-nav nav-item">
                            <a class="nav-link" href="{{ url('/login') }}" data-toggle="modal" data-target="#bd-example-modal">Login</a>
                        </li>
                        <li class="left-nav nav-item ">
                            <a class="nav-link" href="{{ url('/register') }}">Register</a>
                        </li>
                         @endif
                        
                      <li class="nav-item left-nav" title="search" style="padding-right: 40px; margin-top: -10px;"> 
                            <a class="nav-link btn-search"><i class="fa fa-search left-nav-ico" ></i></a>
                        </li>
                    </div>
                </ul>  
            </div>
</nav>

     @if (session('status'))
              <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                 {{ session('status') }}
              </div>
    @endif

		<div class="container">
			<div class="row">
				<div class="col-lg-9">
					<div class="ts-grid-box content-wrapper single-post">
						<div class="entry-header">
							<h2 class="post-title lg">{{$post->posttitle}}</h2>
							<ul class="post-meta-info">
								<li>
									<a href="#" class="post-cat ts-yellow-bg">{{$post->posttags}}</a>
								</li>
								<li class="author">
									<a href="#">
										{{$post->created_by}}
									</a>
								</li>

								<li>
									<i class="fa fa-clock-o"></i>
									<?php echo date('M d, Y',strtotime($post->created_at));?>
								</li>
								<li class="active">
									<i class="fa fa-eye"></i>
									{{$post->view_count}}
								</li>
								<li class="share-post">
									<a href="#comment">
										<i class="fa fa-comment"></i>
										<span style="color: #b3b3b3;"> {{count($post->postcomments)}}</span>
									</a>
								</li>
								<li class="share-post">
									<span style=" font-size: 20px"><a href="{{url('/post/'.$post->id.'/'.$post->posttitle.'/like')}}">
                                                    <i class="fa fa-thumbs-up"></i>
                                                </a>{{$post->likes}}</span>

								</li>
							</ul>
						</div>
						<!-- single post header end-->
						<div class="post-content-area">
						
							<div class="post-media post-featured-image">
								@if(isset($post->youtube_video))
								<iframe style="width: 100%; height: 350px;" src="{!!$post->youtube_video!!}"></iframe>
								@endif
								<br>
                  @if(isset($post->postcoverimage))
								<a href="#" class="gallery-popup">
									<img src="{{asset('post/'.$post->postcoverimage)}}" class="img-fluid cover-image">
								</a>
                  @endif
							</div>
						
							<div class="entry-content" style="font-size: 10px;">

								<br>
								{!!$post->postbody!!}
							</div>
							<!-- entry content end-->
						</div>
						<div style="text-align: right; margin-bottom: 10px;">
							<div style="margin-bottom:-5px;">
									<a class="twitter-share-button" href="https://twitter.com/intent/tweet?text={{$post->posttitle}}"><i class="fa fa-twitter"></i>
Tweet</a></div>
                                               
											
						<!-- Your share button code -->
											  <div class="fb-share-button" 
											    data-href="https://www.spotrum.com/post/{{$post->id}}/{{$post->posttitle}}" 
											    data-layout="button_count">
											  </div>
											   
											  
								</div>
						<!-- post content area-->
						<div class="author-box">
							
							<div class="author-info">
								<h4 class="author-name">{{$post->created_by}}</h4>
								

								<div class="authors-social">
									@if($post->user->social_media != ' ')

									<?php
									 $social_media=json_decode($post->user->social_media,true);
                   if (isset($social_media['facebook'])) {
                      echo '<a href="'.$social_media['facebook'].'" class="ts-facebook">
                    <i class="fa fa-facebook"></i>
                  </a>';
                   }
									
									 if (isset($social_media['twitter'])) {
                     echo '<a href="'.$social_media['twitter'].'" class="ts-twitter">
                    <i class="fa fa-twitter"></i>
                  </a>';
                   }
                   if (isset($social_media['instagram'])) {
                     echo '<a href="'.$social_media['instagram'].'" class="ts-instagram">
                    <i class="fa fa-instagram"></i>
                  </a>';
                   }
									 
									
									?>
									@endif
								</div>
								
								
								<div class="clearfix"></div>
							

							</div>
						</div>
						<!-- author box end-->
						<div class="post-navigation clearfix">
							<div class="post-previous float-left">
								<?php
								$id=$post->id-1;
								if ($id < 1) {
									echo '<span><i class="fa fa-caret-square-o-left"></i> Read Previous</span>';
								}
								else{
								?>
								 <a href="{{url('/post/'.$id.'/'.$post->posttitle)}}">
									
									<span><i class="fa fa-caret-square-o-left"></i> Read Previous</span>
									
								</a>
							<?php } ?>
							</div>
							<div class="post-next float-right">
								<?php
								$id=$post->id+1;
								?>
								 <a href="{{url('/post/'.$id.'/'.$post->posttitle)}}">
									
									<span>Read Next <i class="fa fa-caret-square-o-right"></i></span>
									
								</a>

							</div>
						</div>
						<!-- post navigation end-->
					</div>
					<!--single post end -->
					<section id="comment">
					<div class="comments-form ts-grid-box">
							<h5 class="heading-design-h5">Comments<small> ({{count($comments)}})</small></h5>
						<form role="form" action="{{route('post.comment')}}" method="GET" class="form-inline" style="margin-bottom: 40px;">
							
								
								{{csrf_field()}}
								<input type="hidden" name="id" value="{{$post->id}}">
								<input type="hidden" name="post_user_id" value="{{$post->user_id}}">
								<input type="hidden" name="posttitle" value="{{$post->posttitle}}">
								<div class="col-md-12">
									<div class="row">

										@if(Auth::check())
										@if(isset(Auth::user()->avatar))
											<img src="{{asset('images/profile/thumbnail/'.Auth::user()->avatar)}}" width="50">
										@else
										<span class="span-msg-box">
											{{substr(Auth::user()->name, 0, 1)}} 
										</span>
										@endif
										@endif
										<textarea class="msg-box" id="message" name="comment" placeholder="Add a comment" required=""></textarea>
								<button class="btn-comment" type="submit">Comment</button>
							</div>
						</div>
						
							
						</form>
						
						<!-- Form end -->
					
						@if(isset($comments))
						@foreach($comments as $comment)
						<div class="comment">	
							@if(isset($comment->user->avatar))
								<img src="{{asset('images/profile/thumbnail/'.$comment->user->avatar)}}" width="50">
							@else
								<span class="span-msg-box">
									{{substr($comment->created_by, 0, 1)}} 
								</span>
							@endif
										
								<span style="padding:0px 10px;color: #000; "><b>{{$comment->created_by}} </b></span>
									<span style="font-size: 10px;"><?php echo date('D m, Y H:i a',strtotime($comment->created_at)); ?></span>
								<div style="padding-left: 60px;color: #000;">
								{{$comment->comment}} 
								@if($comment->user_id == Auth::user()->id || Auth::user()->isAdmin == 1)
								<a href="{{url('delete/comment/'.$comment->id)}}" style="float:right; "><i class="fa fa-trash"></i></a>
								@endif
								</div>
						<br>

									<div style="margin-left: 40px;"> 

										@foreach($comment->subcomments as $subcomment)
                    @if($subcomment->privacy_status == 0)
                    @if(isset($subcomment->user->avatar))
                            <img src="{{asset('images/profile/thumbnail/'.$comment->user->avatar)}}"  width="50">
                          @else
                            <span class="span-msg-box" style="font-size: 12px;">
                              {{substr($subcomment->created_by, 0, 1)}} 
                            </span>
                          @endif
                            <span style="padding:0px 10px;color: #000; "><b>{{$subcomment->created_by}} </b></span>
                        <span style="font-size: 10px;"><?php echo date('D m, Y H:i a',strtotime($subcomment->created_at)); ?></span>
                      <div style="padding-left: 60px;color: #000;">
                      {{$subcomment->comment}}
                      @if($subcomment->user_id == Auth::user()->id || Auth::user()->isAdmin == 1)
                      <a href="{{url('delete/subcomment/'.$subcomment->id)}}" style="float:right; "><i class="fa fa-trash"></i></a>
                      @endif
                      </div>

                    @elseif($subcomment->privacy_status == 1)
                    @if($subcomment->user_id == Auth::user()->id || $subcomment->comment_user_id == Auth::user()->id)
                          @if(isset($subcomment->user->avatar))
                            <img src="{{asset('images/profile/thumbnail/'.$comment->user->avatar)}}"  width="50">
                          @else
                            <span class="span-msg-box" style="font-size: 12px;">
                              {{substr($subcomment->created_by, 0, 1)}} 
                            </span>
                          @endif
                            <span style="padding:0px 10px;color: #000; "><b>{{$subcomment->created_by}} </b></span>
                        <span style="font-size: 10px;"><?php echo date('D m, Y H:i a',strtotime($subcomment->created_at)); ?></span>
                      <div style="padding-left: 60px;color: #000;">
                      <i class="fa fa-user-secret" title="This reply is private. Only you can see this message"></i> {{$subcomment->comment}}
                      @if($subcomment->user_id == Auth::user()->id || Auth::user()->isAdmin == 1)
                      <a href="{{url('delete/subcomment/'.$subcomment->id)}}" style="float:right; "><i class="fa fa-trash"></i></a>
                      @endif
                      </div>

                      @endif
                    @endif

      								

										@endforeach
										@if(Auth::check())
										
											<a style="color: #000;margin-left: 10px; font-size: 10px;" onclick="OpenReply({{$comment->id}});"> <i class="fa fa-reply"></i> Reply</a>

											<a style="color: #000;margin-left: 10px; font-size: 10px;" onclick="OpenReplyPri({{$comment->id}});"><i class="fa fa-reply"></i> Reply privately </a>
									

										<form class="subcomment{{$comment->id}}" action="{{route('post.subcomment')}}" method="GET" style="display: none;">
											{{csrf_field()}}
											<input type="hidden" name="id" value="{{$post->id}}">
											<input type="hidden" name="posttitle" value="{{$post->posttitle}}">
											<input type="hidden" name="comment_id" value="{{$comment->id}}">
											<input type="hidden" name="post_user_id" value="{{$post->user_id}}">
											<input type="hidden" name="privacy_status" value="0">
                      <input type="hidden" name="comment_user_id" value="{{$comment->user_id}}">
											<div class="row">
												<div class="col-md-8">
										<input type="text" name="comment" class="msg-box" placeholder="Add a comment">
										</div>
										<div class="col-md-3">
										<button class="btn-comment" type="submit">Comment</button>
										</div>
										</div>
										</form>
										<form class="subcommentpri{{$comment->id}}" action="{{route('post.subcomment')}}" method="GET" style="display: none;">
											{{csrf_field()}}
											<input type="hidden" name="id" value="{{$post->id}}">
											<input type="hidden" name="posttitle" value="{{$post->posttitle}}">
											<input type="hidden" name="comment_id" value="{{$comment->id}}">
											<input type="hidden" name="post_user_id" value="{{$post->user_id}}">
											<input type="hidden" name="privacy_status" value="1">
                      <input type="hidden" name="comment_user_id" value="{{$comment->user_id}}">
											<div class="row">
												<div class="col-md-8">
										<input type="text" name="comment" class="msg-box" placeholder="Add a comment">
										</div>
										<div class="col-md-3">
										<button class="btn-comment" type="submit">Comment</button>
										</div>
										</div>
										</form>
										<br>
											@endif
								</div>
								
						
						<br>
						@endforeach
						@endif
						</div>
						
					
					</section>
				
					<!-- comment form end-->
					<div class="ts-grid-box">
						<h2 class="ts-title">Most Popular</h2>

						<div class="most-populers owl-carousel">
							 @foreach( $popularposts as $post)
							<div class="item">
								<a class="post-cat ts-yellow-bg" href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}">{{$post->posttags}}</a>
								<br>
								<div class="post-content">
									<h3 class="post-title">
										<a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}"> {{$post->posttitle}}</a>
									</h3>
									<span class="post-date-info">
										<i class="fa fa-clock-o"></i>
										<?php echo date('M d, Y',strtotime($post->created_at));?>
									</span>
								</div>
							</div>
							@endforeach
							
						</div>
						<!-- most-populers end-->
					</div>
				</div>
				<!-- col end -->
				 <div class="col-lg-3">
                <div class="right-sidebar">

                    <!-- Login Model here-->
                    <div id="side-twitter">
                    <a class="twitter-timeline" href="https://twitter.com/Spotrum4" data-chrome="nofooter" > Tweets by @Spotrum4 </a>
                </div>


                @if(isset($ads))

                    <div class="widgets widget-banner">
                       <a href="{!!$ads->ad_link!!}" title="{{$ads->ad_owner}}">
                        <img class="img-fluid" src="{{asset('ads/'.$ads->ad_image)}}" alt="Ad">
                        </a>

                    </div>
                    @else
                    <div class="widgets widget-banner">
                        
                        <a data-toggle="modal" data-target="#bd-example-modal" href="#">
                               <img class="img-fluid" src="{{asset('images/banner/Ads-holder.jpg')}}" alt="">
                           </a>

                    </div>
                  @endif
                      <!-- widgets end-->
                    <div class="post-list-item widgets">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation">
                                <a class="active" href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                    <i class="fa fa-clock-o"></i>
                                    Popular tags
                                </a>
                            </li>
                            <li role="presentation">
                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                                    <i class="fa fa-heart"></i>
                                    Popular Posts
                                </a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active ts-grid-box post-tab-list" id="home">
                                  <?php
                            $i=0;
                            foreach($posttags as $tag) { 
                               echo '<div class="post-content media">
                                   
                                    <div class="media-body">
                                            <span class="post-tag" >
                                                 <a style="color:#000;" href="/search/'.$tag->tag.'" >'.$tag->tag.'</a>
                                            </span>
                                        
                                    </div>
                                </div>';
                               $i++;
                               if ($i>=6) {
                                  break;
                               }
                           }
                           ?>
                                

                            </div>
                            <!--ts-grid-box end -->

                            <div role="tabpanel" class="tab-pane ts-grid-box post-tab-list" id="profile">
                                @foreach( $popularposts as $post)
                                <div class="post-content media">
                                   
                                    <div class="media-body">
                                            <span class="post-tag">
                                                <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}"> {{$post->posttags}}</a>
                                            </span>
                                        <h4 class="post-title">
                                            <a href="{{url('/post/'.$post->id.'/'.$post->posttitle)}}">{{str_limit($post->posttitle,65)}} </a>
                                        </h4>
                                    </div>
                                </div>
                               @endforeach
                            </div>
                            <!--ts-grid-box end -->
                        </div>
                        <!-- tab content end-->

                    </div>

              @if(!Auth::check())

                    <div class="widgets widget-banner">
                        
                        <a data-toggle="modal" data-target="#bd-example-modal" href="#">
                               <img class="img-fluid" src="{{asset('images/banner/sidebar-banner1.jpg')}}" alt="">
                        </a>

                    </div>
                    @else
                    <div class="widgets widget-banner">
                        
                        <a href="mailto:contact@spotrum.com?Subject=Request to place ads">
                               <img class="img-fluid" src="{{asset('images/banner/Ads-holder.jpg')}}" alt="">
                           </a>
                    </div>
                    @endif
                    <!-- widgets end-->

                  </div>
              </div>
        </div>
        <!-- row end-->
    </div>
    <!-- container end-->
				
<!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>


 <a data-toggle="modal" data-target="#post-modal" href="{{url('/home')}}"><button class=" new-blog-btn"><img src="{{asset('images/icon/blog.png')}}"></button></a>
<!-- footer area -->
<div class="footer-area">
    <!-- footer start -->
    <footer class="ts-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-menu text-center">
                        <ul>
                           
                            <li>
                                <a href="{{url('/privacy-policy')}}">Privacy</a>
                            </li>
                            
                            <li>
                                <a href="{{url('/terms-and-conditions')}}">Terms and conditions</a>
                            </li>
                        </ul>
                    </div>
                    <div class="copyright-text text-center">
                        <p>&copy; <?php echo date('Y'); ?>, SportForum is a product of Koete Synergy Limited.</p>
                    </div>
                    
                    <div class="copyright-text text-center" ><p>Managed by <a href="https://www.cinoteck.com.ng" style="color:#63B6F3;" target="_blank">Cinoteck</a></p></div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer end -->
</div>
<!-- footer area end-->

<div class="modal fade login-modal-main" id="bd-example-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="login-modal">
                    <div class="row">
                        <div class="col-lg-6 pad-right-0">
                            <div class="login-modal-left">
                                <img src="{{asset('images/login_register_bg.png')}}">
                            </div>
                        </div>
                        <div class="col-lg-6 pad-left-0">
                            <button type="button" class="close close-top-right" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                           
                                <div class="login-modal-right">
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="login" role="tabpanel">
                                            <h5 class="heading-design-h5">Login to your account</h5>
                                             <form method="POST" action="{{ route('login') }}">
                                                  {{csrf_field()}}
                                            <fieldset class="form-group">
                                                <label for="formGroupEmailInput">Enter Email</label>
                                                <input type="email" class="form-control" id="formGroupEmailInput"
                                                       placeholder="johndoe@example.com" name="email">
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label for="formGroupExampleInput2">Enter Password</label>
                                                <input type="password" class="form-control" id="formGroupPasswordInput2"
                                                       placeholder="********" name="password">
                                            </fieldset>
                                              <p><label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                                <input type="checkbox" name="remember" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description" >Remember me </span>
                                            </label>
                                            </p>
                                            <fieldset class="form-group">
                                                <button type="submit" class="btn btn-lg btn-theme-round btn-block">Log in
                                                    to your account
                                                </button>
                                            </fieldset>
                                           <div style="text-align: center;">
                                <a  href="{{ route('password.request') }}" >
                                    Forgot Your Password?
                                </a>
                                </div>
                                        </form>

                                            <div class="login-with-sites text-center">
                                                <p>or Login with your social profile:</p>
                                               <a href="{{url('login/facebook')}}">
                        <button class="btn-facebook login-icons btn-lg">
                            <i class="fa fa-facebook"></i> Facebook
                        </button>
                    </a>
                    <a href="{{url('login/google')}}">
                        <button class="btn-google login-icons btn-lg">
                            <i class="fa fa-google"></i> Google
                        </button>
                    </a>
                                            </div>
                                           
                                        </div>
                                        <div class="tab-pane" id="register" role="tabpanel">
                                            <h5 class="heading-design-h5">Register Now!</h5>
                                            <form method="POST" action="{{ route('register') }}">
                                                {{csrf_field()}}
                                            <fieldset class="form-group">
                                                <label for="formGroupExampleInput">Enter Email</label>
                                                <input type="text" class="form-control" id="formGroupExampleInput" name="email" 
                                                       placeholder="johndoe@example.com" required>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label for="formGroupExampleInput2">Enter Password</label>
                                                <input type="password" class="form-control" id="formGroupExampleInput2" name="password" 
                                                       placeholder="********" required>
                                            </fieldset>
                                            <fieldset class="form-group">
                                                <label for="formGroupExampleInput3">Enter Confirm Password </label>
                                                <input type="password" class="form-control" name="password_confirmation" id="password-confirm"
                                                       placeholder="********" required>
                                            </fieldset>
                                             <p>
                                                <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                                    <input type="checkbox" class="custom-control-input" required>
                                                    <span class="custom-control-indicator"></span>
                                                    <span class="custom-control-description">I Agree with<a href="{{url('/terms-and-conditions')}}"> Term and Conditions </a> </span>
                                                </label>
                                            </p>
                                            <fieldset class="form-group">
                                                <button type="submit" class="btn btn-lg btn-theme-round btn-block">
                                                    Create Your Account
                                                </button>
                                            </fieldset>
                                           
                                        </form>
                                        <div class="login-with-sites text-center">
                                                <p>or Sign up with your social profile:</p>
                                               <a href="{{url('login/facebook')}}">
                        <button class="btn-facebook login-icons btn-lg">
                            <i class="fa fa-facebook"></i> Facebook
                        </button>
                    </a>
                    <a href="{{url('login/google')}}">
                        <button class="btn-google login-icons btn-lg">
                            <i class="fa fa-google"></i> Google
                        </button>
                    </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="text-center login-footer-tab">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" data-toggle="tab" href="#login" role="tab"><i
                                                        class="icofont icofont-lock"></i> LOGIN</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" data-toggle="tab" href="#register" role="tab"><i
                                                        class="icofont icofont-pencil-alt-5"></i> REGISTER</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- New Blog Post Modal-->

<div class="modal fade login-modal-main" id="post-modal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="login-modal">
                    <div class="row">
                      
                        <div class="col-lg-12 pad-left-0">
                            <button type="button" class="close close-top-right" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                           
                                <div class="login-modal-right">
                                     <h5 class="heading-design-h5">Create blog post</h5>
                                     <form action="{{url('post/submit')}}" method="POST" id="post-form" enctype="multipart/form-data"  onsubmit="return checkForm()">
                {{csrf_field()}}
                <div class="blog_img" style="position: relative;text-align: center;">
                  <label>Selet cover image for blog post</label><br>
                            <img src="{{asset('images/banner/temp.jpg')}}" id="preview" class="img-fluid" />
                           
                              <input type="file" name="postcoverimage" accept="image/*" id="image_file" onchange="fileSelectHandler()" title="select cover picture for blog post" class="form-control">
                              <div class="error"></div>
                              <input type="hidden" id="x1" name="x1" />
                              <input type="hidden" id="y1" name="y1" />
                              <input type="hidden" id="x2" name="x2" />
                              <input type="hidden" id="y2" name="y2" />
                              <input type="hidden" id="w" name="w" />
                              <input type="hidden" id="h" name="h" />
                              <input type="hidden" id="filesize" name="filesize" />
                             <input type="hidden" id="filetype" name="filetype" />
                              <input type="hidden" id="filedim" name="filedim" />
                           
                  </div>

                <label>Post Title</label>
                <input type="text" name="posttitle" class="form-control" required placeholder="Post title"><br>
              
<label>Select tag relating to post</label>
    <select required name="posttags" class="form-control">
        <option disabled></option>
        @foreach($posttags as $tag)
        <option value="{{$tag->tag}}"> {{$tag->tag}} </option>
        @endforeach
    </select>
<br>

<label>Add Youtube Video Link</label>
<input type="url" name="youtube_video" class="form-control" placeholder="* optional- enter URL to youtube video">

<input type="hidden" name="postbody" required>
<label>Post</label>
                <div id="editor">
                    
                </div><br>
                <button class="btn btn-lg btn-theme-round btn-block">Publish</button>
            </form>
       
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>


<!--End of New blog Post Modal-->
<!-- javaScript Files
=============================================================================-->

<!-- initialize jQuery Library -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- navigation JS -->
<script src="{{asset('js/navigation.js')}}"></script>
<!-- Popper JS -->
<script src="{{asset('js/popper.min.js')}}"></script>
<!-- Owl Carousel -->
<script src="{{asset('js/owl-carousel.2.3.0.min.js')}}"></script>
<!-- magnific popup JS -->
<script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
<!-- Bootstrap jQuery -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- Owl Carousel -->
<script src="https://cdn.quilljs.com/1.3.6/quill.js"></script>
<script src="{{asset('js/jcrop/js/jquery.Jcrop.min.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
</body>
</html>

