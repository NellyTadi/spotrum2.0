@extends('layouts.app')
@section('body')
<style>
    .dash-top{
    margin:20px 30px;
    background: #fff;
    padding: 15px 15px ;
    -webkit-box-shadow: 7px 6px 22px 2px rgba(230,230,230,1);
-moz-box-shadow: 7px 6px 22px 2px rgba(230,230,230,1);
box-shadow: 7px 6px 22px 2px rgba(230,230,230,1);
    height: auto;
  }
  p{
      padding:15px 25px;
  }
  h3{
      padding:10px;
  }
  h4{
      padding:10px 15px;
  }
  ul.list{ list-style-type: square;padding:0px 40px;color:black;}
</style>
<div class="container ">
    <div class="dash-top">
<h1 style="padding-left:20px;">Spotrum Terms and Conditions</h1>

<p>We have revised Spotrum Terms and conditions, which apply to your use our digital platforms, including website, apps (e.g., mobile apps, tablet apps) and digital services. Some of the key changes are summarized below, but please take the time to read the full Terms of Use. You accept and agree to be bound by these Terms and conditions when you use Spotrum platforms, including, without limitation, when you view or access other content. </p>
<ul class="list"><li>
<b>Governing Law; Arbitration & Class Action Waiver:</b> To help streamline the resolution of disputes, claims, and controversies under these Terms of Use, as set forth in more detail below, you now agree that both you and Spotrum will be obligated to arbitrate disputes, claims, and controversies that arise out of or relate to your use of the Services and/or the provision of content, services, and/or technology on or through the Services, and that any such proceedings will be conducted only on an individual basis.</li></ul>
<h3>HOW WE UPDATE THESE TERMS OF USE</h3>
<p>Spotrum reserves the right, in its sole discretion, to modify or replace this Agreement from time to time, and so you should review this page periodically. When we change the Agreement in a material way, we will update the ‘last updated’ date at the bottom of this page. Your continued use of the Services after any such change constitutes your acceptance of the new terms. If you do not agree to any of these terms or any future terms, do not use or access (or continue to access) the Services.</p>
<h3>USE OF OUR SERVICE</h3>
<p><b>Eligibility</b> Because we respect the rights of children and parents, you may use the Bleacher Report Service only if you can form a binding contract with Bleacher Report, and only in compliance with this Agreement and all applicable local, state, national, and international laws, rules and regulations. The Service is provided for your personal, noncommercial use only. You may not use the Service for any commercial purposes.</p>

<p><b>Spotrum Accounts. </b>You can browse Spotrum and enjoy the Service without registering for a Spotrum account. In order to post any User Content or access certain features of the Service, however, you must register for an account with Spotrum (which may include connecting to Spotrum through a third-party service) and select a password and screen name (“Spotrum User ID”). You may not select or use as a Spotrum User ID any name that we determine to be offensive, vulgar or obscene. Spotrum reserves the right to refuse registration of, or cancel a Spotrum User ID or a User account in its sole discretion. When creating your account, you must provide accurate and complete information. We reserve the right to reclaim Bleacher Report User IDs on behalf of businesses or individuals that hold legal claim or trademark on those usernames. You are responsible for maintaining the confidentiality of your Spotrum password. By connecting to Spotrum with a third-party service like Facebook, you give us permission to access and use your information from that service as permitted by that service, and to store your log-in credentials for that service. For more information on the types of information we collect from these third-party services, please read our Privacy Policy.</p>
<p><b>Your responsibility for your account:</b> You are solely responsible for the activity that occurs on or through your account, and you must keep your account password secure. You must notify Spotrum immediately of any breach of security or unauthorized use of your account. Spotrum will not be liable for your losses caused by any unauthorized use of your account, and you shall be solely liable for the losses of Bleacher Report or others due to such unauthorized use.</p>
<p><b>Groups.</b> Spotrum may allow Users to create or join groups on the Service in order to share articles and other content, and to send SMS text messages to other Group members (a “Group”). If you are added to a Group by a User, you may be sent an SMS text message with a unique hyperlink to directly access your Group’s page. </p>
<p><b>How to control your account.</b> You may control your User profile and how you interact with the Service by changing the settings on your profile page. For more information on how you can control the types of information we collect, please read our Privacy Policy.</p>
<p><b>Your interaction with other Users.</b> You are solely responsible for your interactions with other Users. We reserve the right, but have no obligation, to monitor disputes between you and other Users. Spotrum will have no liability for your interactions with other Users, or for any User’s action or inaction. Please be good to one another.</p>
<p><b>Changes to the Service.</b> Here at Spotrum, we’re always innovating and finding ways to provide our Users with new and innovative features and services. Therefore, Spotrum may, without prior notice, change the Service; stop providing the Service or features of the Service, to you or to Users generally; or create usage limits for the Service. We may permanently or temporarily terminate or suspend your access to the Service without notice and liability for any reason, including if in our sole determination you violate any provision of this Agreement, or for no reason. Upon termination for any reason or no reason, you continue to be bound by this Agreement.</p>
<h3>OUR CONTENT</h3>
<p><b>Spotrum Content.</b> Except for User Content, the Service, and all Intellectual Property Rights therein and related thereto, are the exclusive property of Spotrum and its licensors (“Spotrum Content”). Except as explicitly provided herein, nothing in this Agreement shall be deemed to create a license to the Bleacher Report Content, and you agree not to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publicly perform, publish, adapt, edit or create derivative works from the Spotrum Content, including without limitation any materials or content accessible on the Service. “Bleacher Report,” “Team Stream,” “Real-Time News for Your Favorite Teams,” “You Make The Call”, “The Open Source Sports Network,” “Open Source Sports Network,” “Spotrum” and other Spotrum graphics, logos, designs, page headers, button icons, scripts, and service names are trademarks, trademarks or trade dress of Spotrum protected by the laws of the United States and/or other countries or jurisdictions. Spotrum's trademarks and trade dress may not be used, including as part of trademarks and/or as part of domain names, in connection with any product or service in any manner that is likely to cause confusion. Use of the Spotrum Content or materials on the Service for any purpose not expressly permitted by this Agreement is strictly prohibited.</p>
<p><b>	Our license to you.</b> Subject to the terms and conditions of this Agreement, Spotrum provides you with a license to use the Service for your personal, noncommercial use only and as expressly permitted by the features of the Service. Spotrum may terminate this license at any time for any reason or no reason.</p>
<p><b>Feedback you provide.</b> We value input from our Users, and are always interested in learning of ways we can make Spotrum better. You may choose to or we may invite you to submit comments, ideas or feedback about the Service, including without limitation about how to improve the Service or our products (“Feedback”). By submitting any Feedback, you agree that your disclosure is gratuitous, unsolicited and without restriction and will not place Spotrum under any fiduciary or other obligation, and that we are free to use the Feedback without any additional compensation to you, and/or to disclose the Feedback on a non-confidential basis or otherwise to anyone. You further acknowledge that, by acceptance of your submission, Spotrum does not waive any rights to use similar or related Feedback previously known to Spotrum, or developed by its employees, or obtained from sources other than you.</p>

<h3>PRIVACY</h3>
<p>We care about the privacy of our Users. You understand that by using the Service you consent to the collection, use and disclosure of your personally identifiable information and other information as set forth in our Privacy Policy, and to have such information collected, used, transferred to and processed in the United States. Bleacher Report cannot guarantee that unauthorized third parties will never be able to defeat our security measures. You acknowledge that you provide your information at your own risk.</p>

<h3>INDEMNITY</h3>
<p>You agree to defend, indemnify and hold harmless Bleacher Report, its parents, subsidiaries, agents, affiliates, customers, vendors, officers and employees from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including reasonable attorney’s fees and cost) arising from: (i) your use of and access to the Service; (ii) your violation of any term of this Agreement; (iii) your violation of any third-party right, including without limitation any right of privacy or Intellectual Property Rights; (iv) your violation of any applicable law, rule, or regulation; (v) any claim for damages that arise as a result of any of your User Content or any that is submitted via your account or Spotrum User ID.</p>

<h3>THIRD-PARTY LINKS, SITES AND SERVICES</h3>
<p>The Service may contain links to other websites, advertisers, services, special offers, or other events or activities that are not owned or controlled by Spotrum. Because Spotrum has no control over such sites and resources, you acknowledge and agree that Spotrum is not responsible for the availability of such external sites or resources, and does not endorse and is not responsible or liable for any content, advertising, products or other materials on or available from such sites or resources. You further acknowledge and agree that Spotrum shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such site or resource. You understand that these Terms of Use and our Privacy Policy do not apply to your use of such sites. We encourage you to be aware of when you leave the Service, and to read the terms and conditions and privacy policy of any third-party website or service that you visit.</p>

<h3>TERMINATION OF YOUR ACCOUNT AND THE SERVICE</h3>
<p>Spotrum may terminate or suspend the Service in whole or in part and/or your Spotrum account immediately, without prior notice or liability, for any reason or for no reason, including without limitation, if you breach any of the terms or conditions of this Agreement. Upon termination of your account, your right to use the Service will immediately cease.</p>

<p>If you wish to terminate your Spotrum account, you may discontinue using the Service by sending an email message to [email address] with the words “Terminate account” in the subject field, or by sending mail to the following email address:Customer Support  <a href="mailto:contactus@spotrum.com" target="_top">contactus@spotrum.com</a>
</p>
<p>All provisions of this Agreement, which by their nature should survive termination, shall survive termination, including, without limitation, ownership provisions, warranty disclaimers, indemnity, and limitations of liability.</p>

<h3>GOVERNING LAW; ARBITRATION; CLASS ACTION WAIVER</h3>



<h3>PRIVACY</h3>
<p>We care about the privacy of our Users. You understand that by using the Service you consent to the collection, use and disclosure of your personally identifiable information and other information as set forth in our Privacy Policy, and to have such information collected, used, transferred to and processed in the United States. Bleacher Report cannot guarantee that unauthorized third parties will never be able to defeat our security measures. You acknowledge that you provide your information at your own risk.</p>

<p><b>Class Action Waiver</b></p>
<h4>PLEASE READ THIS SECTION CAREFULLY – IT MAY SIGNIFICANTLY AFFECT YOUR LEGAL RIGHTS.</h4>
<p>
i.	We and you agree that we and you will resolve any disputes, claims, or controversies on an individual basis, and that any claims brought under this Agreement in connection with the Service will be brought in an individual capacity, and not on behalf of, or as part of, any purported class, consolidated, or representative proceeding. We and you further agree that we and you shall not participate in any consolidated, class, or representative proceeding (existing or future) brought by any third party arising under this Agreement in connection with the Service.</p>
<p>ii.	If any court or arbitrator determines that the class action waiver set forth in this section is void or unenforceable for any reason or that arbitration can proceed on a class basis, then the disputes, claims, or controversies will not be subject to arbitration and must be litigated in federal court located in ………..</p>

<h3>MISCELLANEOUS TERMS</h3>
<ul class="list">
<li>	No Agency; Waiver. No agency, partnership, joint venture, or employment is created as a result of this Agreement and you do not have any authority of any kind to bind Bleacher Report in any respect whatsoever. The failure of either party to exercise in any respect any right provided for herein shall not be deemed a waiver of any further rights hereunder.</li>
<li>	Notification. Spotrum may provide notifications, whether such notifications are required by law or are for other business purposes, to you via email notice, “push” notification on your mobile device, written or hard copy notice, or through posting of such notice on the Service, as determined by Spotrum in our sole discretion. Spotrum reserves the right to determine the form and means of providing notifications to Users, provided that you may opt out of certain means of notification as described in this Agreement. Spotrum is not responsible for any automatic filtering you or your network provider may apply to email notifications we send to the email address you provide us.</li> 
<li>	Entire Agreement/Severability. This Agreement, together with any amendments and any additional agreements you may enter into with Bleacher Report in connection with the Service, shall constitute the entire agreement between you and Spotrum concerning the Service. If any provision of this Agreement is found to be unenforceable or invalid, that provision shall be limited or eliminated to the minimum extent necessary so that the Agreement shall otherwise remain in full force and effect and enforceable.</li>
<li>	Assignment. This Agreement is not assignable, transferable or sub-licensable by you except with Spotrum prior written consent. Spotrum may transfer, assign or delegate this Agreement and its rights and obligations without consent.</li></ul>
</div>
</div>
@endsection