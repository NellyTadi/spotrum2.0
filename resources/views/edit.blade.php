@extends('layouts.app')

@section('body')


 
<div class="container" >
    @foreach ($errors->all() as $message) 
       {{$message}}
       @endforeach
    <div class="row">
              <div class="col-md-3" style="padding: 20px;background: #fff;margin:20px 0px;">
      <ul class="nav flex-column">
  <li class="nav-item">
    <a class="nav-link" href="{{url('/')}}">Home</a>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{url('posts/created')}}">My posts</a>
  </li>
  @if(Auth::user()->isAdmin == 1)
  <li class="nav-item">
    <a class="nav-link " href="{{url('all/posts/created')}}">View all posts</a>
  </li>
    <li class="nav-item">
    <a class="nav-link " href="{{url('create/ads')}}">Create new ads <i class="fa fa-plus"></i></a>
  </li>
  @endif
  <li class="nav-item">
    <a class="nav-link" href="{{url('/profile')}}">My profile</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " href="{{route('change.password')}}">Security and privacy</a>
  </li>
</ul>
</div>
       
        <div class="col-md-9" style="padding: 20px;background: #fff;margin:20px 0px;">
              <h5 class="heading-design-h5">Edit blog post</h5>
            <form action="{{route('post.update.submit')}}" method="POST" id="post-form" enctype="multipart/form-data"  onsubmit="return checkForm()">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$post->id}}">
                <div class="blog_img" style="position: relative;text-align: center;">
                  <label>Selet cover image for blog post</label><br>
                            <img src="" id="preview" src="{{asset('post/'.$post->postcoverimage)}}" class="img-fluid" />
                           
                              <input type="file" name="postcoverimage" accept="image/*" id="image_file" onchange="fileSelectHandler()" title="select cover picture for blog post">
                              <div class="error"></div>
                              <input type="hidden" id="x1" name="x1" />
                              <input type="hidden" id="y1" name="y1" />
                              <input type="hidden" id="x2" name="x2" />
                              <input type="hidden" id="y2" name="y2" />
                              <input type="hidden" id="w" name="w" />
                              <input type="hidden" id="h" name="h" />
                              <input type="hidden" id="filesize" name="filesize" />
                             <input type="hidden" id="filetype" name="filetype" />
                              <input type="hidden" id="filedim" name="filedim" />
                           
                  </div>
               
                <label>Post Title</label>
                <input type="text" name="posttitle" class="form-control" required placeholder="Post title" value="{{$post->posttitle}}"><br>
              
                <label>Select tag relating to post</label>
                    <select  required name="posttags" class="form-control">
                        <option disabled></option>
                        <option value="{{$post->posttags}}">{{$post->posttags}}</option>
                        @foreach($posttags as $tag)
                        <option value="{{$tag->tag}}"> {{$tag->tag}} </option>
                        @endforeach
                    </select>
                <br>
                <label>Add Youtube Video Link</label>
<input type="url" name="youtube_video" class="form-control" value="{{$post->youtube_video}}" placeholder="* optional- enter URL to youtube video">
                <input type="hidden" name="postbody" required>
                <label>Post</label>
                                <div id="editor">
                                    {!!$post->postbody!!}
                                </div><br>
                                <button class="btn btn-lg btn-theme-round btn-block">Update</button>
            </form>
        </div>
    </div>
</div>

@endsection